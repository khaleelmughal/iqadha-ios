//
//  AppDelegate.swift
//  iQadha
//
//  Created by NetSet on 3/7/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CoreLocation
import UserNotifications
import Crashlytics
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,CLLocationManagerDelegate{

    var window: UIWindow?
    
    
    
    let locationManager = CLLocationManager()
    var ref: DatabaseReference!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
     
        
      setFirebaseConfig_Environment()
        ref = Database.database().reference()
        UserDefaults.standard.set(nil, forKey: "checkOnline")
        
        let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        
        if let val = UserDefaults.standard.value(forKey: "AuthId") as? String {
            getNotificationStatus()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
                locationManager.startUpdatingLocation()
                locationManager.desiredAccuracy = locationManager.desiredAccuracy
//                locationManager.requestWhenInUseAuthorization()
//                locationManager.requestAlwaysAuthorization()
                locationManager.distanceFilter = 100
            }
            
            
//            if  self.compareTimeDay_Night() {
//
//                setupSplashForLogedInUser(withSplashImage: UIImage(named: "daySplash"))
//
//            }
//
//            else {
//
//               setupSplashForLogedInUser(withSplashImage: UIImage(named: "nightSplash"))
//            }
//
            
           
        
        } else {
      
//
//            if  self.compareTimeDay_Night() {
//
//                setupSplash(withSplashImage: UIImage(named: "daySplash"))
//
//            }
//
//            else {
//
//                setupSplash(withSplashImage: UIImage(named: "nightSplash"))
//            }
//
        }
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        
       
     
        
        UIApplication.shared.statusBarStyle = .lightContent
        return true
    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        print("App Delegate fires...")
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    var valueForNotifyButton = Bool()
    
    func getNotificationStatus() {
        if InternetManager.isConnectedToNetwork() {
            //CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
            let userID = UserDefaults.standard.value(forKey: "AuthId")!
            ref.child("users").child(userID as! String).child("userProfile").observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                self.valueForNotifyButton = value?["qadhaReminder"] as? Bool ?? false
                
                if self.valueForNotifyButton == true {
                    self.fireNotification(chk: true)
                } else {
                    self.fireNotification(chk: false)
                }
                
            }) { (error) in
                
                print(error.localizedDescription)
            }
        } else {
        
        }
    }
    

    
    
    func applicationWillResignActive(_ application: UIApplication) {
        if UserDefaults.standard.value(forKey: "checkOnline") == nil {
            
        } else {
            ref = Database.database().reference()
            let ud = "0"
            UserDefaults.standard.set("\(ud)", forKey: "checkOnline")
            ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["online":false])
        }
    }

    func fireNotification(chk:Bool) {
        if chk {
        let content = UNMutableNotificationContent()
        content.title = "Have you prayed your Qadha today?"
        content.body = "Don’t forget to add it in your logbook."
        content.sound = UNNotificationSound.default()
        
        let gregorian = Calendar(identifier: .gregorian)
        let now = Date()
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
        
        // Change the time to 7:00:00 in your locale
        components.hour = 18
        components.minute = 00
        components.second = 0
        
        let date = gregorian.date(from: components)!
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        let request = UNNotificationRequest( identifier: "identifier", content: content, trigger: trigger)
        print("INSIDE NOTIFICATION")
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error) in
            if let error = error {
                print("SOMETHING WENT WRONG")
            }
        })
        } else {
            let content = UNMutableNotificationContent()
            content.title = "Havfdhfhfghfghfha today?"
            content.body = "Don’t forget to add it in your logbook."
            content.sound = UNNotificationSound.default()
            
            let gregorian = Calendar(identifier: .gregorian)
            let now = Date()
            var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
            
            // Change the time to 7:00:00 in your locale
            components.hour = 25
            components.minute = 00
            components.second = 0
            
            let date = gregorian.date(from: components)!
            
            let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
            
            
            let request = UNNotificationRequest( identifier: "identifier", content: content, trigger: trigger)
            print("INSIDE NOTIFICATION")
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: {(error) in
                if let error = error {
                    print("SOMETHING WENT WRONG")
                }
            })
        }
    }
    
//    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
//    application.applicationIconBadgeNumber = 0
//    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("done")
        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "TabBarId") as! TabbarController
        self.window!.rootViewController = main
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        let curentDate = NSDate()
        let formatter = DateFormatter()
        //        // initially set the format based on your datepicker date
        formatter.dateFormat = "MMMM dd, yyyy EEEE"
        
        let timestamp = Int(curentDate.timeIntervalSince1970 * 1000)
        print(timestamp)
        if UserDefaults.standard.value(forKey: "checkOnline") == nil {
        }
        else {
            ref = Database.database().reference()
            let ud = "1"
            UserDefaults.standard.set("\(ud)", forKey: "checkOnline")
            ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["online":true])
            ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["onlineTime": ServerValue.timestamp()])
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func compareTimeDay_Night() -> Bool{
        let startTime = (4*60)+30 // 4:30
        let endTime =  (17*60)+30 //"17:30"
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)         // 1
        let minutes = calendar.component(.minute, from: date)    // 2
        let seconds = calendar.component(.second, from: date)    // 3
        print("\(hour):\(minutes):\(seconds)")                   // 4
        let calculatedTime = (hour*60)+(minutes)
        if calculatedTime >= startTime && calculatedTime < endTime {
            //its day
//            setupSplash(withSplashImage: UIImage(named: "daySplash"))
           return true
            
        } else {
            // its night
//            setupSplash(withSplashImage: UIImage(named: "nightSplash"))
           return false
        }
        
//        if calculatedTime == ((16*60) + 41) {
////            var localNotification = UILocalNotification()
////            localNotification.alertBody = "Time to pray qadha."
////           // localNotification.timeZone = NSTimeZone.default
////           // localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
////
////            UIApplication.shared.scheduleLocalNotification(localNotification)
//
//            print("notificationnn///////")
//        }
    }
    
    func setupSplash(withSplashImage splashImage: UIImage?) {
        let splashImageView = UIImageView(image: splashImage)
        
        splashImageView.backgroundColor = UIColor.white
        splashImageView.frame = UIScreen.main.bounds
        window?.rootViewController?.view.addSubview(splashImageView)
        window?.rootViewController?.view.bringSubview(toFront: splashImageView)
       
        // aman  delay change
        
        UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseInOut, animations: {() -> Void in
            splashImageView.alpha = 0.0
            let x: CGFloat = -60.0
            let y: CGFloat = -120.0
            splashImageView.frame = CGRect(x: x, y: y, width: splashImageView.frame.size.width - 2 * x, height: splashImageView.frame.size.height - 2 * y)
        }, completion: {(_ finished: Bool) -> Void in
            if finished {
                splashImageView.removeFromSuperview()
               
            }
        })
    }
    
    
    
    func setupSplashForLogedInUser(withSplashImage splashImage: UIImage?) {
        let splashImageView = UIImageView(image: splashImage)
       
 
       
        
        splashImageView.backgroundColor = UIColor.white
        splashImageView.frame = UIScreen.main.bounds
        self.window?.rootViewController?.view.addSubview(splashImageView)
        window?.rootViewController?.view.bringSubview(toFront: splashImageView)
    
        
        
       
        
        // aman  delay change
      
     
        UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseInOut, animations: {() -> Void in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let main = storyboard.instantiateViewController(withIdentifier: "LaunchViewController") as! LaunchViewController
            self.window!.rootViewController = main
            self.window?.backgroundColor = UIColor.clear
            
       
            splashImageView.alpha = 0.0
            let x: CGFloat = -60.0
            let y: CGFloat = -120.0
            splashImageView.frame = CGRect(x: x, y: y, width: splashImageView.frame.size.width - 2 * x, height: splashImageView.frame.size.height - 2 * y)
      
            
        }, completion: {(_ finished: Bool) -> Void in
            if finished {
                
             
                    splashImageView.removeFromSuperview()
              
                
            }
        })
        
    }

    
    func setFirebaseConfig_Environment () {
 //  Fabric.with([Crashlytics.self])
    //    Fabric.sharedSDK().debug = true
        #if DEBUG
        let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info-Debug", ofType: "plist")
        #else
        let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
        #endif
        guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
            fatalError("Invalid Firebase configuration file.")
        }
        FirebaseApp.configure(options: options)
        
    }
    
    
}
