//
//  BaseVC.swift
//  iQadha
//
//  Created by Ranjana Prashar on 3/7/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import EZLoadingActivity


class BaseVC: UIViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Email Validation
    func checkEmailValidation(_ string: String) -> Bool {
        let stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", stricterFilterString)
        return emailTest.evaluate(with: string)
    }
    
    func displayAlert( userMessage: String,  completion: (() -> ())? = nil) {
        var topVC = UIApplication.shared.keyWindow?.rootViewController
        while((topVC!.presentedViewController) != nil){
            topVC = topVC!.presentedViewController
        }
        let alertView = UIAlertController(title: appName.iQ_appName.rawValue, message: userMessage, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            if completion != nil {
                completion!()
            }
        }))
        topVC?.present(alertView, animated: true, completion: nil)
    }

}
