//
//  CommonFunctions.swift
// Restaurant
//
//  Created by Divya Khanna on 9/23/16.
//  Copyright © 2016 Divya Khanna. All rights reserved.
//

import Foundation
import UIKit
import SwiftLoader
import AVKit
import AVFoundation


class CommonFunctions {
    class var sharedCommonFunctions : CommonFunctions {
        struct Static {
            static let instance : CommonFunctions = CommonFunctions()
        }
        return Static.instance
    }
    
    

    func activateView(_ view: UIView, loaderText: String) {
        view.isUserInteractionEnabled = false
        SwiftLoader.show(title: loaderText, animated: true)
    }
    
    func inactivateView(_ view: UIView) {
        view.isUserInteractionEnabled = true
        DispatchQueue.main.async() {
            SwiftLoader.hide()
            // Hide Loading Indicator 
        }
    }
    
    // MARK: - Show Alert
    func showAlert(_ view:UIViewController,title:String,message:String) {
        
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            view.present(alert, animated: true, completion: nil)
    }
    }
