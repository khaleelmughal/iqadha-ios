//
//  GlobalObjectClass.swift
//
//  Created by NetSet on 05/04/17.
//  Copyright © 2017 NetSet. All rights reserved.
//

import UIKit

enum appName: String {
    case iQ_appName = "iQadha"
}

class GlobalConstantClass: NSObject
{
    struct ScreenSize {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    }
    
    struct Color {
        static let iQ_AppColor: UIColor = UIColor(red: 92.0/255.0, green: 179.0/255.0, blue: 144.0/255.0, alpha: 1.0)
        static let iQ_AppColorLightGray: UIColor = UIColor.lightGray.withAlphaComponent(0.4)
    }
    
    
    
    struct APIConstantNames {
      // static let baseUrl            =   "http://192.168.2.101:8080/Adeeb/"   //LOCAL
       static let baseUrl             =    "http://112.196.97.229:8080/Adeeb/"
       //  static let baseUrl             =  "http://adeebserver.live:8080/Adeeb/"
      //  static let baseUrl             =    "http://ec2-18-195-58-123.eu-central-1.compute.amazonaws.com:8080/Adeeb/"
        
    }
}

extension UIButton {
    func cornerRadius() {
        self.layer.cornerRadius = 6.0
        self.clipsToBounds = true
    }
}

extension UITextField {
    func underlined()  {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let layer: CALayer = CALayer()
                layer.borderColor = UIColor.lightGray.cgColor
                layer.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
                layer.borderWidth = 1
                self.layer.addSublayer(layer)
                self.clipsToBounds = true
                // self.setValue(GlobalConstantClass.Color.k_AppBlueColor, forKeyPath: "_placeholderLabel.textColor")
            }
        }
    }
    
    func setImageLeftSideOnTextfield(image: UIImage)  {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: self.frame.size.height))
        imageView.contentMode = .center
        imageView.image = image
        self.leftView = imageView
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func setImageRightSideOnTextfield(image: UIImage)  {
        let imageViewError = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height))
        imageViewError.contentMode = .right
        imageViewError.image = image
        self.rightView = imageViewError
        self.rightViewMode = UITextFieldViewMode.always
    }
}
