//
//  LaunchViewController.swift
//  iQadha
//
//  Created by netset on 5/25/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var lauchImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDel = UIApplication.shared.delegate as! AppDelegate
        
        
        if  appDel.compareTimeDay_Night() {
            lauchImage.image = UIImage(named: "daySplash")
        }
            
        else {
            lauchImage.image = UIImage(named: "nightSplash")
        }
    
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
       
            
            if let val = UserDefaults.standard.value(forKey: "AuthId") as? String {
                
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let main = storyboard.instantiateViewController(withIdentifier: "TabBarId") as! TabbarController
                appDel.window!.rootViewController = main
                appDel.window?.backgroundColor = UIColor.clear
                
                
            } else {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let main = storyboard.instantiateViewController(withIdentifier: "firstNavBar") 
                appDel.window!.rootViewController = main
                appDel.window?.backgroundColor = UIColor.clear
                
                
            }
            
            
            
        }
        
        

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
