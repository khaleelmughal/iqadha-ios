//
//  Tag.swift
//  TagFlowLayout
//
//  Created by Shapaves on 3/14/18.
//  Copyright © 2018 Shapaves. All rights reserved.
//

class Tag {    
    var name: String?
    var selected = false
    var image : String?
    var prayerCount : Int?
}
