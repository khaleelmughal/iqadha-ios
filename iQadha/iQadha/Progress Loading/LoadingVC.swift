//
//  LoadingVC.swift
//  iQadha
//
//  Created by NetSet on 3/16/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import HGCircularSlider

class LoadingVC: UIViewController {
    var totalCount = Int()
    let valueInitiallyAdded = 15
    var incrementedValue : Int = 1
   // var circularView = EFCircularSlider()
    
     var circularView = KDCircularProgress()
    @IBOutlet weak var viewRoundWidthConst: NSLayoutConstraint!
    @IBOutlet weak var viewRoundHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewRound: UIView!
    @IBOutlet weak var labelTotalCount: UILabel!
    @IBOutlet weak var imgViewRound: UIImageView!
   
    var rotationTimer:Timer!
    
    override func viewDidLoad() {
        totalCount = userInfoGlobalDict["totalQadha"] as! Int
//        totalCount=totalCount+valueInitiallyAdded
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularView = KDCircularProgress(frame:viewRound.frame)
        circularView.progressThickness = 0.2
        circularView.clockwise = true
        circularView.gradientRotateSpeed = 2
        circularView.roundedCorners = false
        circularView.glowMode = .forward
        circularView.glowAmount = 0.9
       circularView.progressInsideFillColor = UIColor.clear
         circularView.set(colors: UIColor.init(hexString: "#4BD4A2"))
        circularView.trackColor = UIColor.clear
        self.circularView.backgroundColor = .clear
        view.addSubview(circularView)
    
        circularView.startAngle = 270
        
           var counterValue = totalCount / 60
        
        if  counterValue != 0 {
        rotationTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(LoadingVC.loading), userInfo: nil, repeats: true)
        }
        
        else {
           rotationTimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(LoadingVC.loading2), userInfo: nil, repeats: true)
            
        }
        
            
        circularView.animate(fromAngle: 0, toAngle:360, duration: 6.0) { (success) in
            self.rotationTimer.invalidate()
           
            
            self.labelTotalCount.text = String(self.totalCount)
            
             print(self.labelTotalCount.text,self.totalCount)
            self.totalCountAnimationOnCompletion()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
//                self.performSegue(withIdentifier: "stepTwoVC", sender: nil)
//            }
            
  
            
        }
    

    }
    
    
    @objc func loading() {
       
        
        if Int(labelTotalCount.text!)! < totalCount {
           var count = 0
                var counterValue = totalCount / 60
            print(counterValue)
                labelTotalCount.text = String(Int(labelTotalCount.text!)! + counterValue)
            }
        
            
            }
    
    
    @objc func loading2() {
        
        if Int(labelTotalCount.text!)! < totalCount {
            var count = totalCount / 6
            labelTotalCount.text = String(Int(labelTotalCount.text!)! + count)
            count = count + count
        }
        
        
    }
    
    
    
    

    
//    else {
//    var count = 1
//    labelTotalCount.text = String(Int(labelTotalCount.text!)! + count)
//
//    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
//    count += 1;
//    }
//
//
//    }
    
    func roundCalculationAnimation() {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: {
            self.incrementedValue = self.incrementedValue+self.valueInitiallyAdded
            self.valueChanged()
        }, completion: {(finished: Bool) in
            if Int(self.labelTotalCount.text!)! < self.totalCount-self.valueInitiallyAdded{
                self.roundCalculationAnimation()
            } else {
                self.labelTotalCount.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                UIView.animate(withDuration: 0.0010, delay: 0, usingSpringWithDamping: 0.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: { [weak self] in
                    self?.labelTotalCount.transform = .identity
                    },  completion: {(finished: Bool) in
                        UIView.animate(withDuration: 0.0010, delay: 0.0, options: .curveEaseInOut, animations: {
                            self.viewRoundHeightConst.constant = self.view.frame.size.width
                            self.viewRoundWidthConst.constant = self.viewRoundHeightConst.constant
                            self.imgViewRound.alpha=0.0
                            self.circularView.alpha=0.0
                            self.view.layoutIfNeeded()
                        }, completion:{(finished: Bool) in 
                            self.performSegue(withIdentifier: "stepTwoVC", sender: nil)
                    })
                })
            }
        })
    }
    func valueChanged() {
        var extraIncrementedValue : Int = 0
        if self.incrementedValue >  self.totalCount-self.valueInitiallyAdded {
            extraIncrementedValue =  self.incrementedValue-self.totalCount+self.valueInitiallyAdded
            self.incrementedValue = self.incrementedValue-extraIncrementedValue
        }
        
        var value:Int! = 1
        
        
        
        if String(totalCount).count == 6 {
        
           value = 10
        }
        
        
        if String(totalCount).count == 3 {
        
            value = 1
        }
        
        

//        else if String(totalCount).count == 5 {
//            self.circularView.currentValue = Float(incrementedValue * 2)
//            labelTotalCount.text = String(incrementedValue * 2)
//        }
//
//
//        else if String(totalCount).count == 4 {
//            self.circularView.currentValue = Float(incrementedValue +  10)
//            labelTotalCount.text = String(incrementedValue +  10)
//        }
//
//        else if String(totalCount).count == 3 {
//            self.circularView.currentValue = Float(incrementedValue +  3)
//            labelTotalCount.text = String(incrementedValue +  3)
//        }

//
//
//
//
//        else {
//        self.circularView.currentValue = Float(incrementedValue * value)
        labelTotalCount.text = String(incrementedValue * value)
//        }
    }
    
    
    func totalCountAnimationOnCompletion() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.labelTotalCount.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }) { (true) in
            
            UIView.animate(withDuration: 0.5, animations: {
                self.labelTotalCount.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })  { (true) in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.viewRound.transform = CGAffineTransform(scaleX: 3.5, y: 3.5)
//                      self.imgViewRound.transform = CGAffineTransform(scaleX: 1.8, y: 1.8)
                     self.circularView.transform = CGAffineTransform(scaleX: 3.5, y: 3.5)
                    
                }) { (true) in
                   
                    UIView.animate(withDuration: 0.01, animations: {
                          self.viewRound.alpha = 0.0
                        self.circularView.alpha = 0.0
                        
                    }, completion: { (true) in
                     self.performSegue(withIdentifier: "stepTwoVC", sender: nil)
                    })
                    
                }
                
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
      
       
    }
}
