//
//  SignInVC.swift
//  iQadha
//
//  Created by NetSet on 3/7/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignInVC: BaseVC {
    
    @IBOutlet weak var vwLine: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnDOneFrgtPswrd: UIButton!
    @IBOutlet weak var lblMsgSent: UILabel!
    @IBOutlet weak var btnSaveInputData: UIButton!
    @IBOutlet weak var InnerWhiteVw: UIView!
    @IBOutlet var blurrVW: UIView!
    @IBOutlet weak var lblForgotPswrdEmail: UILabel!
    @IBOutlet weak var txtFldForgotPswrd: UITextField!
    @IBOutlet weak var textFldPassword: UITextField!
    @IBOutlet weak var textFldEmailAddress: UITextField!
    
    var testArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
 
        
//
//        #if DEBUG
////            textFldEmailAddress.text = "qwerty@gmil.com"
////            textFldPassword.text = "qwerty@123"
//        #else
//
//        #endif
        
        self.setupPopUpViews()
        // Do any additional setup after loading the view.
    }
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        blurrVW.frame = rect
        self.view.addSubview(blurrVW)
        InnerWhiteVw.setViewCorner()
        blurrVW.isHidden = true
        btnSaveInputData.setCorner()
        btnDOneFrgtPswrd.setCorner()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionDone(_ sender: Any) {
        blurrVW.isHidden = true
    }
    @IBAction func actionForgotPassword(_ sender: Any) {
        
      //  testArray.removeObject(at: -1)
        blurrVW.isHidden = false
        btnDOneFrgtPswrd.isHidden = true
        lblMsgSent.isHidden = true
        
        vwLine.isHidden = false
        self.txtFldForgotPswrd.text! = ""
        btnCross.isHidden = false
        btnSaveInputData.isHidden = false
        lblForgotPswrdEmail.isHidden = false
        txtFldForgotPswrd.isHidden = false
    }
    
    @IBAction func actionSendEmailForgotPassword(_ sender: Any) {
        
         txtFldForgotPswrd.resignFirstResponder()
        
        if txtFldForgotPswrd.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterEmail)
        } else if !checkEmailValidation(txtFldForgotPswrd.text!) {
            super.displayAlert(userMessage: Validation.call.validEmail)
        } else {
            
            CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
           
            
            Auth.auth().sendPasswordReset(withEmail: "\(txtFldForgotPswrd.text!)") { error in
                // Your code here
                if error == nil {
                    
                    self.btnDOneFrgtPswrd.isHidden = false
                    self.lblMsgSent.isHidden = false
                    
                    self.vwLine.isHidden = true
                   // self.btnCross.isHidden = true
                    self.btnSaveInputData.isHidden = true
                   // self.lblForgotPswrdEmail.isHidden = true
                    self.txtFldForgotPswrd.isHidden = true
                    
                    DispatchQueue.main.async() {
                        // Hide Loading Indicator
                        CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                    }
                } else {
                    
                    DispatchQueue.main.async() {
                        // Hide Loading Indicator
                        CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                    }
                    
                    
                    let err = (error?.localizedDescription) as! String
                    super.displayAlert(userMessage:"\(err)")
                }
            }
        }
    }
    
    @IBAction func actionCross(_ sender: Any) {
      txtFldForgotPswrd.resignFirstResponder()
        
        blurrVW.isHidden = true
    }
    
    @IBAction func btnSignInAct(_ sender: Any) {
        self.view.endEditing(true)
        if textFldEmailAddress.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterEmail)
        } else if !checkEmailValidation(textFldEmailAddress.text!) {
            super.displayAlert(userMessage: Validation.call.validEmail)
        } else if textFldPassword.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterPassword)
        }
//        else if (textFldPassword.text?.count)!<6 {
//            super.displayAlert(userMessage: Validation.call.countPassword)
//        }
        else {
        
//            let objStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
//            let objVC : TabbarController = (objStoryboard.instantiateViewController(withIdentifier: "TabBarId") as? TabbarController)!
//            objVC.selectedIndex = 0
//            self.present(objVC, animated: true, completion: nil)
            signIn()
        }
    }
    
    func signIn() {
        

        if InternetManager.isConnectedToNetwork() {
            
            CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        Auth.auth().signIn(withEmail: self.textFldEmailAddress.text!, password: self.textFldPassword.text!) { (user, error) in
    
            if error == nil {
                
                UserDefaults.standard.removeObject(forKey: "AuthId")
                UserDefaults.standard.set( "\((user?.user.uid)!)", forKey: "AuthId")
                UserDefaults.standard.synchronize()
                
                let objStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
                let objVC : TabbarController = (objStoryboard.instantiateViewController(withIdentifier: "TabBarId") as? TabbarController)!
                objVC.selectedIndex = 0
                self.present(objVC, animated: true, completion: nil)
                
            } else {
                DispatchQueue.main.async() {
                    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                }
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
         }
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    @IBAction func btnSignUpAct(_ sender: Any) {
        self.view.endEditing(true)
        self.performSegue(withIdentifier: "signUpVC", sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

extension SignInVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = GlobalConstantClass.Color.iQ_AppColor.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = GlobalConstantClass.Color.iQ_AppColorLightGray.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
        if (textField.text?.count)! == 0 && string == " " {
            return false
        }
        if textField==textFldEmailAddress {
            if (textField.text?.count)! >= 40 && range.length == 0 {
                return false
            }
        } else if textField==textFldPassword {
            if (textField.text?.count)! >= 20 && range.length == 0 {
                return false
            }
        }
        return true
    }
}
