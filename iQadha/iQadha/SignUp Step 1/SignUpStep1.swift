//
//  SignUpStep1.swift
//  iQadha
//
//  Created by Ranjana Prashar on 3/8/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SignUpStep1: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var arrayforDays = NSArray()
    var arraSecondDay = NSArray()
    var arrayforMonths = NSArray()
    var arrayforyears = NSMutableArray()
    var secondYearArray = [String]()
    var yearValue = Int()
    var str = NSString()
    
    
    
    var selectedDay = ""
    var selectedMonth = ""
    var selectedYear = ""
    
    
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var ddmmyyLbl: UILabel!
    @IBOutlet weak var dobView: UIView!
    
    @IBOutlet weak var balighDate: UILabel!
    @IBOutlet weak var balighddmmyy: UILabel!
    @IBOutlet weak var balighView: UIView!
    
    @IBOutlet weak var dayTable: UITableView!
    @IBOutlet weak var yearTable: UITableView!
    @IBOutlet weak var monthTable: UITableView!
    
    @IBOutlet weak var dayBtn: UIButton!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var yearBtn: UIButton!
    
    @IBOutlet weak var nextBTN: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet var DobGesture: UIButton!
    @IBOutlet var BalighGesture: UIButton!
    
    @IBOutlet weak var DisplyDOB: UILabel!
    
    @IBOutlet var instructView: UIView!
    @IBOutlet weak var viewInputWhite: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        str = "DOB"
        dayBtn.setTitle("Day", for: .normal)
        // BalighGesture.isEnabled = false
        monthBtn.setTitle("Month", for: .normal)
        yearBtn.setTitle("Year", for: .normal)
        
        arrayforDays = ["1", "2", "3", "4", "5", "6","7","8","9", "10", "11", "12", "13","14","15", "16", "17", "18", "19", "20","21","22", " 23", "24", "25", "26","27","28", " 29", "30", "31"]
        //        arraSecondDay = ["1", " 2", "3", "4", "5", "6","7","8","9", "10", "11", "12", "13","14","15", "16", "17", "18", "19", "20","21","22", " 23", "24", "25", "26","27","28", " 29", "30", "31"]
        
        arrayforMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        
//        arrayforyears = ["1925","1926","1927","1928","1929","1930","1931","1932","1933","1934","1935","1936","1937","1938","1939","1940","1941","1942","1943","1944","1945","1946","1947","1948","1949","1950","1951","1952","1953","1954","1955","1956","1957","1958","1959","1960","1961","1962","1963","1964","1965","1966","1967","1968","1969","1970","1971","1972","1973","1974","1975","1976","1977","1978","1979","1980","1981","1982","1983","1984","1985","1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996","1997","1998","1999","2000","2001","2002","2003","2004","2005","2006","2007","2008","2009"]
      
        
        InitializeArrayAccToDate() //
        
        nextBTN.layer.masksToBounds = true
        nextBTN.layer.cornerRadius = 25.0
        nextBTN.isHidden = true
        dayTable.isHidden = true
        monthTable.isHidden = true
        yearTable.isHidden = true
        dayBtn.isHidden = true
        monthBtn.isHidden = true
        yearBtn.isHidden = true
        doneBtn.isHidden = true
        DisplyDOB.isHidden = true
        
//        for i in 1...31 {
//            arrayforDays.adding("\(i)")
//        }
//        for j in 1960...2000 {
//            arrayforMonths.adding("\(j)")
//        }
//        for k in 2001...2016 {
//            arrayforyears.adding("\(k)")
//        }
//
        
        print(arrayforyears)
        
        
        //instructView.isHidden = true
        self.setupPopUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let dob = userInfoGlobalDict["dob"] {
            ddmmyyLbl.text =  userInfoGlobalDict.value(forKey: "dob") as! String
            nextBTN.isHidden = false
            //            DobGesture.isEnabled = true
            //            BalighGesture.isEnabled = true
        }
        
        if let balighDate = userInfoGlobalDict["balighDate"] {
            balighddmmyy.text =  userInfoGlobalDict.value(forKey: "balighDate") as! String
     
            
            
            
            
            
            
            
        }
        
        
    }
    
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        instructView.frame = rect
        self.view?.addSubview(instructView)
        viewInputWhite.setViewCorner()
        instructView.isHidden = true
    }
    
    @IBAction func actionCross(_ sender: Any) {
        instructView.isHidden = true
    }
    
    @IBAction func dayBtnAtn(_ sender: Any) {
        
        dayTable.isHidden = false
        monthTable.isHidden = true
        yearTable.isHidden = true
        
        //        doneBtn.isHidden = true
        //        dayBtn.isSelected = true
        //        yearBtn.isSelected = false
        //        monthBtn.isSelected = false
        //        nextBTN.isHidden = true
        //        dayTable.isHidden = false
        //        monthTable.isHidden = true
        //        yearTable.isHidden = true
        
        dayBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        monthBtn.backgroundColor = UIColor.white
        yearBtn.backgroundColor = UIColor.white
        
        dayBtn.isSelected = true
        monthBtn.isSelected = false
        yearBtn.isSelected = false
    }
    
    @IBAction func monthBtnAct(_ sender: Any) {
        
        dayTable.isHidden = true
        monthTable.isHidden = false
        yearTable.isHidden = true
        
        
        
        
        //        doneBtn.isHidden = true
        //        monthBtn.isSelected = true
        //        yearBtn.isSelected = false
        //        dayBtn.isSelected = false
        //        nextBTN.isHidden = true
        //        dayTable.isHidden = true
        //        monthTable.isHidden = false
        //        yearTable.isHidden = true
        
        dayBtn.backgroundColor = UIColor .white
        monthBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        yearBtn.backgroundColor = UIColor.white
        
        dayBtn.isSelected = false
        monthBtn.isSelected = true
        yearBtn.isSelected = false
    }
    
    @IBAction func yearBtnAct(_ sender: Any) {
        
        dayTable.isHidden = true
        monthTable.isHidden = true
        yearTable.isHidden = false
        
        
        //        doneBtn.isHidden = true
        //        yearBtn.isSelected = true
        //        dayBtn.isSelected = false
        //        monthBtn.isSelected = false
        //        nextBTN.isHidden = true
        //        dayTable.isHidden = true
        //        monthTable.isHidden = true
        //        yearTable.isHidden = false
        
        //        dayBtn.isSelected = false
        //        yearBtn.isSelected = true
        //        monthBtn.isSelected = false
        
        
        dayBtn.backgroundColor = UIColor .white
        monthBtn.backgroundColor = UIColor.white
        yearBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        
        dayBtn.isSelected = false
        monthBtn.isSelected = false
        yearBtn.isSelected = true
        
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        //        //  balighddmmyy.text = "\(dayBtn.titleLabel?.text ?? "") \(monthBtn.titleLabel?.text ?? ""), \(yearBtn.titleLabel?.text ?? "")"
        //        //  ddmmyyLbl.text = "\(dayBtn.titleLabel?.text ?? "") \(monthBtn.titleLabel?.text ?? ""), \(yearBtn.titleLabel?.text ?? "")"
        //
        //        dayBtn.titleLabel?.textColor = UIColor.white
        //        monthBtn.titleLabel?.textColor =  UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        //        yearBtn.titleLabel?.textColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        //        monthBtn.isSelected = true
        //        yearBtn.isSelected = false
        //        dayBtn.isSelected = false
        //
        //        dayTable.isHidden = true
        //        monthTable.isHidden = true
        //        yearTable.isHidden = true
        //        dayBtn.isHidden = true
        //        monthBtn.isHidden = true
        //        yearBtn.isHidden = true
        //        doneBtn.isHidden = true
        //        DisplyDOB.isHidden = true
        //
        //
        //        if (str == "DOB") {
        //            ddmmyyLbl.text = "\(dayBtn.titleLabel?.text ?? "") \(monthBtn.titleLabel?.text ?? ""), \(yearBtn.titleLabel?.text ?? "")"
        //            BalighGesture.isEnabled = true
        //            //_DOBBtn.backgroundColor = UIColor.white
        //            secondYearArray.removeAll()
        //            //            arrayforyears.removeAllObjects()
        //            yearValue = Int((yearBtn.titleLabel?.text)!)!
        //            for k in 0...arrayforyears.count {
        //                let val = yearValue + 8 + k
        //                if val > 2018
        //                {
        //                    break;
        //                }
        //                secondYearArray.append(String(val))
        //
        //            }
        //            //            for k in Int(yearBtn.titleLabel?.text ?? "") ?? 0...2016 {
        //            //                var i = Int(yearBtn.titleLabel?.text ?? "") ?? 0 + 9
        //            //                if k > i {
        //            //                    secondYearArray.append("\(k)")
        //            //                }
        //            //            }
        //
        //            print("\(secondYearArray)")
        //            yearBtn.setTitle("Year", for: .normal)
        //            //  _BalighDateBtn.isEnabled = true
        //        } else {
        //            balighddmmyy.text = "\(dayBtn.titleLabel?.text ?? "") \(monthBtn.titleLabel?.text ?? ""), \(yearBtn.titleLabel?.text ?? "")"
        //            // _BalighDateBtn.backgroundColor = UIColor.white
        //            DobGesture.isEnabled = true
        //            yearBtn.setTitle("Year", for: .normal)
        //            nextBTN.isHidden = false
        //            //  _DOBBtn.isEnabled = true
        //        }
        //        yearTable.reloadData()
        
    }
    
    @IBAction func dobAction(_ sender: Any) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 1 {
            return arrayforDays.count
        }
        else if tableView.tag == 2 {
            return arrayforMonths.count
        }
        else if tableView.tag == 3 {
            
            
            if (str == "DOB") {
                return arrayforyears.count
            } else  {
                return secondYearArray.count
            }
            
        }
        //        else if tableView.tag == 4 {
        //            return 4
        //        }
        //        else {
        return 0
        //        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let CellIdentifier = "cell"
            var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
            }
            cell?.textLabel?.textColor = UIColor.white
            cell?.textLabel?.textAlignment = .center
            cell?.textLabel?.font = UIFont(name: "ProximaNova-Regular", size: 16)
            cell?.backgroundColor = UIColor.clear
            cell?.selectionStyle = .none
            cell?.textLabel?.text = arrayforDays[indexPath.row] as? String
            if let aCell = cell {
                return aCell
            }
            return UITableViewCell()
        }else if tableView.tag == 2 {
            let CellIdentifier = "cell"
            var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
            }
            cell?.textLabel?.textColor = UIColor.white
            cell?.backgroundColor = UIColor.clear
            cell?.textLabel?.textAlignment = .center
            cell?.textLabel?.font = UIFont(name: "ProximaNova-Regular", size: 16)
            cell?.selectionStyle = .none
            cell?.textLabel?.text = arrayforMonths[indexPath.row] as? String
            if let aCell = cell {
                return aCell
            }
            return UITableViewCell()
        } else if tableView.tag == 3 {
            let CellIdentifier = "cell"
            var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
            }
            cell?.textLabel?.textColor = UIColor.white
            cell?.backgroundColor = UIColor.clear
            cell?.selectionStyle = .none
            cell?.textLabel?.textAlignment = .center
            cell?.textLabel?.font = UIFont(name: "ProximaNova-Regular", size: 16)
            if (str == "DOB") {
                cell?.textLabel?.text = arrayforyears[indexPath.row] as? String
            } else {
                cell?.textLabel?.text = secondYearArray[indexPath.row]
            }
            if let aCell = cell {
                return aCell
            }
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Remove seperator inset
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            dayTable.isHidden = true
            dayBtn.setTitle("\(arrayforDays[indexPath.row])", for: .normal)
          
             selectedDay = arrayforDays[indexPath.row] as! String
           
            
            doneBtn.isHidden = true
            monthBtn.isSelected = true
            yearBtn.isSelected = false
            dayBtn.isSelected = false
            nextBTN.isHidden = true
            dayTable.isHidden = true
            monthTable.isHidden = false
            yearTable.isHidden = true
            dayBtn.backgroundColor = UIColor .white
            monthBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
            yearBtn.backgroundColor = UIColor.white
            
            
        }else if tableView.tag == 2 {
            monthTable.isHidden = true
            
            let selectedValue = arrayforMonths[indexPath.row] as! String
           
              selectedMonth = arrayforMonths[indexPath.row] as! String
            
            monthBtn.setTitle(selectedValue, for: .normal)
            doneBtn.isHidden = true
            yearBtn.isSelected = true
            dayBtn.isSelected = false
            nextBTN.isHidden = true
            dayTable.isHidden = true
            monthTable.isHidden = true
            yearTable.isHidden = false
            dayBtn.backgroundColor = UIColor .white
            monthBtn.backgroundColor = UIColor.white
            dayBtn.isSelected = false
            yearBtn.isSelected = true
            monthBtn.isSelected = false
            yearBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
//            yearTable.reloadData()
        }
        else if tableView.tag == 3 {
            yearTable.isHidden = true
            
            
            yearBtn.setTitle("\(arrayforyears[indexPath.row])", for: .normal)
            doneBtn.isHidden = false
            if (str == "DOB") {
                yearStrr = "\(arrayforyears[indexPath.row])"
                selectedYear = arrayforyears[indexPath.row] as! String
                
                 yearBtn.setTitle("\(arrayforyears[indexPath.row])", for: .normal)
            } else {
                yearStrr = "\(secondYearArray[indexPath.row])"
                
                 selectedYear = secondYearArray[indexPath.row] as! String
                
                yearBtn.setTitle("\(secondYearArray[indexPath.row])", for: .normal)
            }
            
        }
   
        
  
      doneButton()
        
    }
    
    var yearStrr = String()
    
    func doneButton() {
   print(selectedDay,selectedMonth,selectedYear)
        
        if (str == "DOB") {
            
            if selectedMonth == "" || selectedDay == "" || selectedYear == "" {
                doneBtn.isHidden = true
                return
            }
          
            if  checkIfDateIsValid() == false {
                
             CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "Please select valid date.")
            doneBtn.isHidden = true
                
                dobView.backgroundColor = UIColor.white
                dayTable.isHidden = true
                monthTable.isHidden = true
                yearTable.isHidden = true
                dayBtn.isHidden = true
                monthBtn.isHidden = true
                yearBtn.isHidden = true
                doneBtn.isHidden = true
                DisplyDOB.isHidden = true
                BalighGesture.isEnabled = true
                ddmmyyLbl.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
                dobLbl.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
                ddmmyyLbl.text = "DD/MM/YY"
                
           return
                
            }
            
            ddmmyyLbl.text = "\(selectedDay) \(selectedMonth), \(selectedYear)"
            dayBtn.titleLabel?.textColor = UIColor.white
            monthBtn.titleLabel?.textColor =  UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
            yearBtn.titleLabel?.textColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
            monthBtn.isSelected = true
            yearBtn.isSelected = false
            dayBtn.isSelected = false
            
            dayTable.isHidden = true
            monthTable.isHidden = true
            yearTable.isHidden = true
            dayBtn.isHidden = true
            monthBtn.isHidden = true
            yearBtn.isHidden = true
            doneBtn.isHidden = true
            DisplyDOB.isHidden = true
        

            
            
            dobView.backgroundColor = UIColor.white
             ddmmyyLbl.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
              dobLbl.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
            
            
            BalighGesture.isEnabled = true
            if secondYearArray.count > 0 {
           secondYearArray.removeAll()
            }
            
          
            let date = Date()
            let calender = Calendar.current
            let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
            let year = components.year
            
            
            yearValue = Int((yearStrr))!
            for k in 0...arrayforyears.count {
                let val = yearValue + 9 + k
                if val > year! {
                    break;
                }
                secondYearArray.append(String(val))
            }
            print("\(secondYearArray)")
            yearBtn.setTitle("Year", for: .normal)
        } else {
           
            
            if selectedMonth == "" || selectedDay == "" || selectedYear == "" {
//                 balighddmmyy.text = "\(selectedDay) \(selectedMonth), \(selectedYear)"
                doneBtn.isHidden = true
                return
            }
           
            if  checkIfDateIsValid() == false {
                
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "Please select Valid Date.")
                doneBtn.isHidden = true
                
                balighView.backgroundColor = UIColor.white
                dayTable.isHidden = true
                monthTable.isHidden = true
                yearTable.isHidden = true
                dayBtn.isHidden = true
                monthBtn.isHidden = true
                yearBtn.isHidden = true
                doneBtn.isHidden = true
                DisplyDOB.isHidden = true
                  DobGesture.isEnabled = true
                
                balighddmmyy.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
                balighDate.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
                balighddmmyy.text = "DD/MM/YY"
                
                return
                
            }
            
            
            
            
            doneBtn.isHidden = true
            
            dayTable.isHidden = true
            monthTable.isHidden = true
            yearTable.isHidden = true
            dayBtn.isHidden = true
            monthBtn.isHidden = true
            yearBtn.isHidden = true
            doneBtn.isHidden = true
            DisplyDOB.isHidden = true
            
            //MARK:- for color

            balighView.backgroundColor = UIColor.white
            balighddmmyy.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
            balighDate.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
            
            
            
            
               balighddmmyy.text = "\(selectedDay) \(selectedMonth), \(selectedYear)"
//            balighddmmyy.text = balighddmmyy.text
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM, yyyy"
            let dateBaligh = dateFormatter.date (from: balighddmmyy.text!)
            
            let dateBirth = dateFormatter.date (from: ddmmyyLbl.text!)
            yearTable.reloadData()
            compareTwoDates(enteredBalighDate: dateBaligh!, enteredBirthDate: dateBirth!)
        }
        yearTable.reloadData()
    }
    
    @IBAction func DobAction(_ sender: Any) {
        str  = "DOB";
        
     //  scrollAllTableToStartIndex()
        selectedDay = ""
        selectedMonth = ""
        selectedYear = ""
        nextBTN.isHidden = true
        balighddmmyy.text = "DD/MM/YY"
        yearStrr = ""
        dayBtn.setTitle("Day", for: .normal)
        monthBtn.setTitle("Month", for: .normal)
        yearBtn.setTitle("Year", for: .normal)
        
        BalighGesture.isEnabled = false
        DisplyDOB.text = "Date of Birth"
        yearTable.reloadData()
        DisplyDOB.isHidden = false
        ddmmyyLbl.textColor = UIColor .white
        dobView.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        dobLbl.textColor = UIColor.white
        monthBtn.isHidden = false
        yearBtn.isHidden = false
        dayBtn.isHidden = false
        dayTable.isHidden = false
        doneBtn.isHidden = true
        balighDate.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
        dayBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        balighView.backgroundColor = UIColor .white
        balighddmmyy.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
        yearBtn.isSelected = false
        dayBtn.isSelected = true
        monthBtn.isSelected = false
        dayBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        monthBtn.backgroundColor = UIColor .white
        yearBtn.backgroundColor = UIColor .white
    }
    
    @IBAction func BalighAction(_ sender: Any) {
     
        selectedMonth = ""
        selectedDay = ""
        selectedYear = ""
        
        if ddmmyyLbl.text! == "DD/MM/YY" {
            
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "Select Date of Birth First.")
            return
            
        }
        
        //        if DisplyDOB.text == "Baligh Date"  {
        //
        //        } else {
        //
        //        }
      
        
       
        
        nextBTN.isHidden = true
        DobGesture.isEnabled = false
        str  = "Baligh";
      //  scrollAllTableToStartIndex()
        dayBtn.setTitle("Day", for: .normal)
        monthBtn.setTitle("Month", for: .normal)
        yearBtn.setTitle("Year", for: .normal)
        yearTable.reloadData()
        DisplyDOB.isHidden = false
        DisplyDOB.text = "Baligh Date(Puberty)"
        dayTable.isHidden = false
        monthTable.isHidden = true
        yearTable.isHidden = true
        dayBtn.isHidden = false
        monthBtn.isHidden = false
        yearBtn.isHidden = false
        balighDate.textColor = UIColor .white
        balighView.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        balighddmmyy.textColor = UIColor .white
//        ddmmyyLbl.textColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        dobView.backgroundColor = UIColor .white
        dobLbl.textColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha:  1.0)
        yearBtn.isSelected = false
        dayBtn.isSelected = true
        monthBtn.isSelected = false
        dayBtn.backgroundColor = UIColor(red: 75.0/255, green: 212.0/255, blue:163.0/255, alpha: 0.7)
        monthBtn.backgroundColor = UIColor .white
        yearBtn.backgroundColor = UIColor .white
        
    }
    
    @IBAction func nextBtnAct(_ sender: Any) {
        self.view.endEditing(true)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        // let dateDOB = dateFormatter.date (from: ddmmyyLbl.text!)
        let dateBaligh = dateFormatter.date (from: balighddmmyy.text!)
        let dateBirth = dateFormatter.date (from: ddmmyyLbl.text!)
        
        
        print((dateBaligh?.years(from: dateBirth!))!)
        
        
        //        if (dateBaligh?.years(from: dateBirth!))! < 9 {
        //            nextBTN.isHidden = true
        //                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "You are not Baligh.")
        //                return
        //        }
        
        var curentDate = NSDate()
        let ref = Database.database().reference()
        // ref.setValue(ServerValue.timestamp())
        
        ref.observe(.value, with: {
            snap in
            if let t = snap.value as? TimeInterval {
                // Cast the value to an NSTimeInterval
                // and divide by 1000 to get seconds.
                curentDate = NSDate(timeIntervalSince1970: t/1000)
            }
        })
        
        let totalDays = countNoOfDays(firstDate: dateBaligh as! NSDate, secondDate: curentDate as! NSDate)
        let totalQadhas = totalDays * 6
        
        print(ddmmyyLbl.text,balighddmmyy.text)
        userInfoGlobalDict.setValue(ddmmyyLbl.text, forKey: "dob")
        userInfoGlobalDict.setValue(balighddmmyy.text, forKey: "balighDate")
        userInfoGlobalDict.setValue(totalQadhas, forKey: "totalQadha")
        
        
        self.performSegue(withIdentifier: "loadingVC", sender: nil)
    }
    
    @IBAction func btnInfoAct(_ sender: Any) {
        //showPopUp()
        instructView.isHidden = false
    }
    
    func countNoOfDays(firstDate:NSDate , secondDate:NSDate) -> Int{
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: firstDate as Date)
        let date2 = calendar.startOfDay(for: secondDate as Date)
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        print(components.day!)
        return components.day!
    }
    
    func showPopUp() {
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(false),
                    .scrollable(false),
                    .movesAlongWithKeyboard(false)
                ]
            )
            .didShowHandler { popup in
        }
        let container = InformationVC.instance()
        container.closeHandler = {
            popup.dismiss()
        }
        popup.show(container)
    }
    
    
    func compareTwoDates(enteredBalighDate:Date,enteredBirthDate:Date) {
        
        DobGesture.isEnabled = true
        yearBtn.setTitle("Year", for: .normal)
        nextBTN.isHidden = false
        
        let dateNow = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        let result = formatter.string(from:dateNow)
        let currentDate = formatter.date (from: result)
        
        if (enteredBalighDate.years(from: enteredBirthDate)) < 9 {
            nextBTN.isHidden = true
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "You are not Baligh.")
            balighddmmyy.text = "DD/MM/YY"
            return
        }
            
        else if enteredBalighDate.compare(currentDate!) == .orderedDescending   {
            nextBTN.isHidden = true
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Iqadha", message: "Selected date is not valid as it seems to be the future date for your baligh date.")
            balighddmmyy.text = "DD/MM/YY"
            return
        }
        
    }
    
    @IBAction func po(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    //MARK: Initialize Array According To Date
    func InitializeArrayAccToDate() {
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: date)
        let year = components.year
        if arrayforyears.count > 0 {
            arrayforyears.removeAllObjects()
        }
        for i in 1925...year! - 9 {
       arrayforyears.add(String(i))
        }
      
  // fill second year array
        if let balighDate = userInfoGlobalDict["balighDate"] {
            if secondYearArray.count > 0 {
                secondYearArray.removeAll()
            }
          let dob = userInfoGlobalDict.value(forKey: "dob") as! String
          var yearDob = dob.split(separator: ",")
            let selectedYear = yearDob[1].trimmingCharacters(in: .whitespaces)
            
            let startIndex = Int(selectedYear)! + 9
            
            for k in startIndex...year! {
                secondYearArray.append(String(k))
        }
        
    }
    
}
    
    //MARK: check If Date is Valid:--
    func checkIfDateIsValid() -> Bool {
       let index = arrayforMonths.index(of: selectedMonth)
        var components = DateComponents()
        components.month = Int(index + 1)
        components.year = Int(selectedYear)
        components.day = Int(selectedDay)
        print(components.day,components.month,components.year)
        components.calendar = Calendar.current
        print(components.isValidDate) // false
        if components.isValidDate {
            return true
        }
        return false
        
    }
    //MARK: scroll Table To Start Index
    func scrollAllTableToStartIndex() {
        if arrayforDays.count > 0 {
       let index = NSIndexPath(item: 0, section: 0)
            dayTable.scrollToRow(at: index as IndexPath, at: .top, animated: false)
            
        }
        
        if arrayforMonths.count > 0 {
            let index = NSIndexPath(item: 0, section: 0)
            monthTable.scrollToRow(at: index as IndexPath, at: .top, animated: false)
        }
        if (str == "DOB") {
            if arrayforyears.count > 0 {
                let index = NSIndexPath(item: 0, section: 0)
                yearTable.scrollToRow(at: index as IndexPath, at: .top, animated: false)
                
            }
        } else  {
            if secondYearArray.count > 0 {
                let index = NSIndexPath(item: 0, section: 0)
                yearTable.scrollToRow(at: index as IndexPath, at: .top, animated: false)
                
            }
        }
        
    }

}


extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    
}

