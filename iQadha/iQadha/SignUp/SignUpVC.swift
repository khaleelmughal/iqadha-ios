//
//  SignUpVC.swift
//  iQadha
//
//  Created by Ranjana Prashar on 3/8/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

var userInfoGlobalDict = NSMutableDictionary()

class SignUpVC: BaseVC {
    @IBOutlet weak var textFldEmailAddress: UITextField!
    @IBOutlet weak var textFldPassword: UITextField!
    @IBOutlet weak var btnForFemale: UIButton!
    @IBOutlet weak var btnForMale: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
    var strForGender = String()
    
    @IBAction func btnSelectGenderAct(_ sender: Any) {
        self.view.endEditing(true)
        let btnSelctedGender = sender as! UIButton
        if btnSelctedGender==self.btnForMale{
            strForGender = "Male"
            self.setButtons(boolBtnSelectedHighLight: true, btnForMaleColor: GlobalConstantClass.Color.iQ_AppColor, btnForFemaleColor: .clear)
        } else {
            strForGender = "Female"
            self.setButtons(boolBtnSelectedHighLight: false, btnForMaleColor: .clear, btnForFemaleColor: GlobalConstantClass.Color.iQ_AppColor)
        }
    }
    
    func setButtons(boolBtnSelectedHighLight:Bool, btnForMaleColor:UIColor, btnForFemaleColor:UIColor)  {
        self.btnForMale.isSelected = boolBtnSelectedHighLight
        self.btnForFemale.isSelected = !boolBtnSelectedHighLight
        self.btnForMale.backgroundColor = btnForMaleColor
        self.btnForFemale.backgroundColor = btnForFemaleColor
        self.btnForFemale.isHighlighted = boolBtnSelectedHighLight
        self.btnForMale.isHighlighted = !boolBtnSelectedHighLight
    }
    
    @IBAction func btnSignInAct(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var checkPswrd: Bool = false
    
    @IBAction func btnSignUpAct(_ sender: Any) {
        self.view.endEditing(true)
        
       checkPswrd = isPasswordValid(textFldPassword.text!)
        
       if textFldEmailAddress.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterEmail)
        } else if !checkEmailValidation(textFldEmailAddress.text!) {
            super.displayAlert(userMessage: Validation.call.validEmail)
        } else if textFldPassword.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterPassword)
       }  else if (textFldPassword.text?.count)!<6 {
            super.displayAlert(userMessage: Validation.call.countPassword)
       } else if checkPswrd == false {
            super.displayAlert(userMessage: Validation.call.strongPassword)
       } else if !btnForMale.isSelected && !btnForFemale.isSelected {
            super.displayAlert(userMessage: Validation.call.selectGender)
        } else {
        
        
        
        
//        let dict = ["email":textFldEmailAddress.text! , "password":textFldPassword.text! , "gender":strForGender]
//        UserDefaults.standard.set(dict, forKey: "signUpDict")
//        UserDefaults.standard.synchronize()
        
        checkUserExisistanceOnServer()
        

        }
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[0-9]).{6,10000}$")
        return passwordTest.evaluate(with: password)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func checkUserExisistanceOnServer() {
        if InternetManager.isConnectedToNetwork() {
            CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        Auth.auth().fetchProviders(forEmail:textFldEmailAddress.text!, completion: {
            (providers, error) in
             CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            if let error = error {
                print(error.localizedDescription)
                  CommonFunctions.sharedCommonFunctions.showAlert(self, title:"iQadha", message:error.localizedDescription)
            } else if let providers = providers {
              
       CommonFunctions.sharedCommonFunctions.showAlert(self, title:"iQadha", message:"User Already exist with same email address.")
                
            }
            else {
                userInfoGlobalDict.setValue(self.textFldEmailAddress.text!, forKey: "email")
                userInfoGlobalDict.setValue(self.textFldPassword.text!, forKey: "password")
                userInfoGlobalDict.setValue(self.strForGender, forKey: "gender")
      
                
                
              
                
               self.performSegue(withIdentifier: "signUpStep1VC", sender: nil)
            }
            
        })
        
    }
        else {
         CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            
        }
        
        
    }
    
    
    
}





extension SignUpVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = GlobalConstantClass.Color.iQ_AppColor.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = GlobalConstantClass.Color.iQ_AppColorLightGray.cgColor
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
        if (textField.text?.count)! == 0 && string == " " {
            return false
        }
        if textField==textFldEmailAddress {
            if (textField.text?.count)! >= 40 && range.length == 0 {
                return false
            }
        } else if textField==textFldPassword {
            if (textField.text?.count)! >= 20 && range.length == 0 {
                return false
            }
        }
        return true
    }
}
