//
//  StepTwoVC.swift
//  iQadha
//
//  Created by NetSet on 3/16/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import CoreLocation

class StepTwoVC: BaseVC ,CLLocationManagerDelegate{

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var valueDataTextField: UITextField!
    
    @IBOutlet var instructView: UIView!
    @IBOutlet weak var viewInputWhite: UIView!
    
    var selectedData = [Tag]()
    var totalCount = Int()
    var ref: DatabaseReference!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      valueDataTextField.delegate = self
          print(self.view.frame)
        
        ref = Database.database().reference()
         self.setupPopUpViews()
//        self.locationManager.requestAlwaysAuthorization()
//        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.startUpdatingLocation()
        }
        totalCount = userInfoGlobalDict["totalQadha"] as! Int
        let arrayData : [NSDictionary] = [["name":"Fajr", "image":"Sun", "count":totalCount / 6],
                                 ["name":"Zuhr", "image":"fullSun", "count":totalCount / 6],
                                 ["name":"Asr", "image":"sunCloud", "count":totalCount / 6],
                                 ["name":"Maghrib", "image":"orangeSun", "count":totalCount / 6],
                                 ["name":"Isha", "image":"moonCloud", "count":totalCount / 6] ,
                                 ["name":"Witr", "image":"moon", "count":totalCount / 6]]
        stackView.isHidden=true
        for data : NSDictionary in arrayData  {
            let tag = Tag()
            tag.name = data.value(forKey: "name") as? String
            tag.image = data.value(forKey: "image") as? String
            tag.prayerCount = data.value(forKey: "count") as? Int
            self.selectedData.append(tag)
        }
    }
    
    var lngLongStr:String = "0.00 , 0.00"
    
    @IBAction func actionBack(_ sender: Any) {
       // self.navigationController?.popViewController(animated: true)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpStep1") as! SignUpStep1
        self.navigationController?.pushViewController(secondViewController, animated: false)
    }
    
   
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        instructView.frame = rect
        self.view?.addSubview(instructView)
        viewInputWhite.setViewCorner()
        instructView.isHidden = true
    }
    
    @IBAction func actionCross(_ sender: Any) {
        instructView.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        print("step 2 Fires.....")
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude),\(locValue.longitude)")
        lngLongStr = "\(locValue.latitude) , \(locValue.longitude)"
    }
    
    @IBAction func applyAct(_ sender: Any) {
        
        print(totalCount)
        
        if valueDataTextField.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.validQadhaPrayer)
        }
            else if Int(valueDataTextField.text!)! > totalCount / 6 {
                  super.displayAlert(userMessage: Validation.call.validQadhaPrayer )
            }
      else {
           self.view.endEditing(true) // dismiss keyboard
            stackView.isHidden = true
            for data : Tag in self.selectedData  {
                if data.selected {
                    if valueDataTextField.text?.count == 0 {
                        data.prayerCount = 0
                        valueDataTextField.text = "0"
                    } else {
                        data.prayerCount = Int(valueDataTextField.text!)
                    }
                }
            }
            
         
            for i in 0..<selectedData.count {
             selectedData[i].selected = false
                
            }
            
            collectionView .reloadData()
        }        
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.view.endEditing(true)
        doneBtn.isUserInteractionEnabled = false
        signUp()

    }
    
    var curentDate = NSDate()
    
    func signUp() {
        
        if InternetManager.isConnectedToNetwork() {
            
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
            
        Auth.auth().createUser(withEmail: userInfoGlobalDict["email"] as! String, password: userInfoGlobalDict["password"] as! String) { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                
                let values = ["online":true ,
                              "totalQadha":"\(self.totalCount)" ,
                              "deviceType":"iOS" ,
                              "onlineTime": 0 ,
                              "userProfile":["balighDate":userInfoGlobalDict["balighDate"] as! String ,
                                             "dob":userInfoGlobalDict["dob"] as! String ,
                                             "email":userInfoGlobalDict["email"] as! String ,
                                             "gender":userInfoGlobalDict["gender"] as! String ,
                                             "language":"english" ,
                                             "location":"\(self.lngLongStr)" ,
                                             "name":"" ,
                                             "phoneNumber":"" ,
                                             "qadhaReminder":true ,
                                             "userid" : Auth.auth().currentUser?.uid]]
                    as [String : Any]
            
                let valuesForPrayers = ["data":
                   ["1":["prayCount":"\(String(describing: self.selectedData[0].prayerCount!))" , "prayName":"Fajr"] ,
                    "2":["prayCount":"\(String(describing: self.selectedData[1].prayerCount!))" , "prayName":"Zuhr"] ,
                    "3":["prayCount":"\(String(describing: self.selectedData[2].prayerCount!))" , "prayName":"Asr"]  ,
                    "4":["prayCount":"\(String(describing: self.selectedData[3].prayerCount!))" , "prayName":"Magrib"] ,
                    "5":["prayCount":"\(String(describing: self.selectedData[4].prayerCount!))" , "prayName":"Isha"] ,
                    "6":["prayCount":"\(String(describing: self.selectedData[5].prayerCount!))" , "prayName":"Witr"]]
                ]

                let valuesForFasts = ["data":["1":["prayCount":"0" , "prayName":"Fasts Qadha"] ]]
                
                self.ref = Database.database().reference()
                self.ref.observe(.value, with: {
                    snap in
                    if let t = snap.value as? TimeInterval {
                        self.curentDate = NSDate(timeIntervalSince1970: t/1000)
                    }
                })
                print(self.curentDate)
                let formatter = DateFormatter()
                var dateStr = String()
                formatter.dateFormat = "dd MMM, yyyy"
                dateStr = formatter.string(from: self.curentDate as Date)
                let valuesForActivity = ["\(dateStr as! String)" :["prayCount":0]]
                
                self.ref.child("users").child((user?.user.uid)!).setValue(values)
                self.ref.child("prayers").child((user?.user.uid)!).setValue(valuesForPrayers)
                self.ref.child("fasts").child((user?.user.uid)!).setValue(valuesForFasts)
                self.ref.child("activityLog").child((user?.user.uid)!).setValue(valuesForActivity)
                
                UserDefaults.standard.removeObject(forKey: "AuthId")
                UserDefaults.standard.set( "\((user?.user.uid)!)", forKey: "AuthId")
                UserDefaults.standard.synchronize()
                
                self.doneBtn.isUserInteractionEnabled = true
                
                let objStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
                let objVC : TabbarController = (objStoryboard.instantiateViewController(withIdentifier: "TabBarId") as? TabbarController)!
                objVC.selectedIndex = 0
                self.present(objVC, animated: true, completion: nil)
                
            } else {
                self.doneBtn.isUserInteractionEnabled = true
                DispatchQueue.main.async() {
                    // Hide Loading Indicator
                    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                }
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
          }
        } else {
            self.doneBtn.isUserInteractionEnabled = true
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    @IBAction func infoAction(_ sender: Any) {
        //showPopUp()
         instructView.isHidden = false
    }

    func showPopUp() {
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(false),
                    .scrollable(false),
                    .movesAlongWithKeyboard(false)
                ]
            )
            .didShowHandler { popup in
        }
        let container = InformationVC.instance()
        container.closeHandler = {
            popup.dismiss()
        }
        popup.show(container)
    }
    
    var checkCount:Int = 0
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension StepTwoVC : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : StepTwoCustomCellClass =  collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! StepTwoCustomCellClass
        self.configureCell(cell, forIndexPath: indexPath)
        return cell
    }
    
    func configureCell(_ cell: StepTwoCustomCellClass, forIndexPath indexPath: IndexPath) {
        let tag = self.selectedData[indexPath.row]
        cell.iconImage.image = UIImage.init(named: tag.image!)
        cell.labelName.text = tag.name
        cell.labelCount.text = String(tag.prayerCount!)
        cell.backgroundColor = tag.selected ? UIColor.init(hexString: "#4BD4A2") : UIColor.white
        cell.labelName.textColor  = tag.selected ? UIColor.white : UIColor.init(hexString: "#5CB390")
        cell.labelCount.textColor = tag.selected ? UIColor.white : UIColor.init(hexString: "#5CB390")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        for data : Tag in self.selectedData  {
            data.selected = false
        }
        let tag = self.selectedData[indexPath.row]
        valueDataTextField.text = String(tag.prayerCount!)
        
        checkCount = Int(tag.prayerCount!)
        
        stackView.isHidden = false
        self.selectedData[indexPath.row].selected = !self.selectedData[indexPath.row].selected
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let spacingInLines = 13
        let width = (Int(collectionView.frame.size.width)-(spacingInLines*2))/3
        return CGSize(width:width, height:width-25)
    }
}

class StepTwoCustomCellClass : UICollectionViewCell{
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
}

extension StepTwoVC : UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          view.endEditing(true)
        textField.resignFirstResponder()
       print(self.view.frame)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
   print(self.view.frame)
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)) {
            return false
        }
        if (textField.text?.count)! == 0 && string == " " {
            return false
        }
        if (valueDataTextField.text?.hasPrefix("0"))! {
            valueDataTextField.text = ""
        }
        if textField==valueDataTextField {
            if (textField.text?.count)! >= 15 && range.length == 0 {
                return false
            }
        }
        return true
    }
}
