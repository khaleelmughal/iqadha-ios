//
//  FastsVC.swift
//  iQadha
//
//  Created by netset on 3/28/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

var goToSettings:Bool = false

class FastsVC: UIViewController {
    @IBOutlet var btnAddFasts: UIButton!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var imgTop: UIImageView!
    @IBOutlet var viewBlurr: UIView!
    @IBOutlet var viewInputWhite: UIView!
    @IBOutlet var btnGo: UIButton!
    
    var ref: DatabaseReference!
    var onVwPryrValue = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        self.navigationController?.isNavigationBarHidden = true
        navigationItem.title = "iQadha"
        btnAddFasts.layer.cornerRadius = 22.0
        viewBlurr.isHidden = true
        setupPopUpViews()
        viewBlurr.isHidden = true
        
        NotificationCenter.default.removeObserver(Notification.Name("editFastsValues"))
        NotificationCenter.default.addObserver(self, selector: #selector(FastsVC.reloadPrayerValues), name: Notification.Name("editFastsValues"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let isDayMode = (UIApplication.shared.delegate as! AppDelegate).compareTimeDay_Night()
        btnAddFasts.backgroundColor = isDayMode ? UIColor.init(hexString: "#fbc742") : UIColor.init(hexString: "#6a549d")
        btnGo.backgroundColor = isDayMode ? UIColor.init(hexString: "#fbc742") : UIColor.init(hexString: "#6a549d")
        imgTop.image = isDayMode ? #imageLiteral(resourceName: "DayImage") : #imageLiteral(resourceName: "moonUp")
        getFastsData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var arrCountOfFasts = NSMutableArray()
    var updatedFastsStr = String()
    
    @objc func reloadPrayerValues(sender:Notification) {
     
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        let reqArr = sender.object as! NSArray!
        updatedFastsStr =  String(reqArr![0] as! Int)
        if Int(updatedFastsStr as! String)! > Int(onVwPryrValue)! {

        } else {
            
            if InternetManager.isConnectedToNetwork() {
            let reqVal = Int(onVwPryrValue)! - Int(updatedFastsStr as! String)!
            var valuesForFasts = ["data":
                        ["1":["prayCount":"\(reqVal as! Int)" , "prayName":"Fasts Qadha"]]
                    ]
            
            self.ref.child("fasts").child(userID as! String).updateChildValues(valuesForFasts) { (error, ref) in
                if error == nil {
                    
                    self.getFastsData()
                    
                } else{
                    CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                    print("error \(String(describing: error))")
                }
            }
            
            } else {
                DispatchQueue.main.async() {
                    // Hide Loading Indicator
                    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                }
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            }
        }
    }
    
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        viewBlurr.frame = rect
        self.tabBarController?.view.addSubview(viewBlurr)
        viewInputWhite.setViewCorner()
        viewBlurr.isHidden = true
        btnGo.setCorner()
    }
    
    @IBAction func btnAddFastAct(_ sender: Any) {
        self.showPopUp()

    }
    
    func getFastsData() {
        
        if InternetManager.isConnectedToNetwork() {
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("fasts").child(userID as! String).child("data").observeSingleEvent(of: .value, with: { (snapshot) in
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                let dic = rest.value as! [String: Any]
                print(dic)
                self.onVwPryrValue = "\(dic["prayCount"] as! String)"
                self.lblCount.text! = "\(dic["prayCount"] as! String)"
            }
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            
            if self.lblCount.text! == "0" {
                self.viewBlurr.isHidden = false
            }
            
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
          }
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    @IBAction func crossAction(_ sender: Any) {
        viewBlurr.isHidden = true
    }
    
    @IBAction func actionGoToSetting(_ sender: Any) {
        goToSettings = true
        let objStoryboard = UIStoryboard(name: "TabBar", bundle: nil)
        let objVC : TabbarController = (objStoryboard.instantiateViewController(withIdentifier: "TabBarId") as? TabbarController)!
        objVC.selectedIndex = 4
        self.present(objVC, animated: true, completion: nil)
    }
    
    
    func showPopUp() {
        let popup = PopupController
            .create(self.tabBarController!)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.6)),
                    .dismissWhenTaps(false),
                    .scrollable(false),
                    .movesAlongWithKeyboard(false)
                ]
            )
            .didShowHandler { popup in
        }
        let container = EditPrayersVC.instance()
        var arrayToPass = NSMutableArray()
        arrayToPass.add(lblCount.text!)
        container.maximumValues_HomeArray = arrayToPass
        container.closeHandler = {
            popup.dismiss()
        }
        popup.show(container)
    }
}
