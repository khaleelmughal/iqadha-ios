//
//  MapChartVC.swift
//  iQadha
//
//  Created by NetSet on 3/26/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit

class ActivityVC: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewIndicator: UIView!
    @IBOutlet weak var constraintsIndicator: NSLayoutConstraint!
    @IBOutlet weak var viewChartContanier: UIView!
    @IBOutlet weak var viewMapContainer: UIView!
    
    @IBOutlet weak var btnChart: UIButton!
    @IBOutlet weak var btnLive: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "ACTIVITY"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
        
        btnChart.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: .normal)
        
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
    
        stackView.subviews.forEach({element in
            (element as! UIButton).setTitleColor(.lightGray, for: .normal)
        })
        sender.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: .normal)
        
        switch sender {
        case btnChart:
            viewChartContanier.isHidden = false
            viewMapContainer.isHidden = true
            constraintsIndicator.constant =  constraintsIndicator.constant - sender.frame.width
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TabChangedToChart"), object: nil, userInfo: nil)
            
            break
        case btnLive:
            viewChartContanier.isHidden = true
            viewMapContainer.isHidden = false
            constraintsIndicator.constant =  constraintsIndicator.constant + sender.frame.width
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "TabChangedToMap"), object: nil, userInfo: nil)
            break
        default:
            break
        }
        viewIndicator.setNeedsUpdateConstraints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
