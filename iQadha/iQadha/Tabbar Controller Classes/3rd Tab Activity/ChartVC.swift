//
//  ChartVC.swift
//  iQadha
//
//  Created by NetSet on 4/6/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Charts
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ChartVC: UIViewController, ChartViewDelegate {
    
    @IBOutlet weak var stackViewTopBtn: UIStackView!
    @IBOutlet weak var lblPrayedLastMonth: UILabel!
    @IBOutlet weak var lblPrayedThisMonth: UILabel!
    @IBOutlet weak var lblComparison: UILabel!
    @IBOutlet var viewGraph: CombinedChartView!
    @IBOutlet weak var lblTotalPrayed: UILabel!
    let arrWeek = ["SUN","MON","TUS","WED","THU","FRI","SAT"]
    var arrMonth = ["WEEK 1","WEEK 2","WEEK 3","WEEK 4"]
    var arrYear = ["Jan","FAB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"]

    
    var shouldHideData: Bool = false
    
    var arrDays = NSArray()
    var maxValue = Double()
    var minVAlue = Double()
    var arrQadha = [Int]()
    //var arrFasts = [Double]()
    var countForXaxis = Int()
    var curentDate = NSDate()
    var ref: DatabaseReference!
    var arrOfDatesOfWeek = NSMutableArray()
    
//    let value1 = [0, 26, 24, 18]
//    let value2 = [33,23, 32,80]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
//        let startWeek = Date().startOfWeek
//        let endWeek = Date().endOfWeek
//        print(startWeek ?? "not found start date")
//        print(endWeek ?? "not found end date")
//        let datesBetweenArray = generateDatesArrayBetweenTwoDates(startDate: startWeek! , endDate: endWeek!)
//        for i in 0..<datesBetweenArray.count {
//            let formatter = DateFormatter()
//            var dateStr = String()
//            formatter.dateFormat = "dd MMM,yyyy"
//            dateStr = formatter.string(from: datesBetweenArray[i] as Date)
//            arrOfDatesOfWeek.add(dateStr)
//        }
        
        let calendar = Calendar.current
        let dateInWeek = Date()//7th June 2017
        let dayOfWeek = calendar.component(.weekday, from: dateInWeek)
        let weekdays = calendar.range(of: .weekday, in: .weekOfYear, for: dateInWeek)!
        let days = (weekdays.lowerBound ..< weekdays.upperBound)
            .flatMap { calendar.date(byAdding: .day, value: $0 - dayOfWeek, to: dateInWeek)
        }
        for i in 0..<days.count {
            let formatter = DateFormatter()
            var dateStr = String()
            formatter.dateFormat = "dd MMM, yyyy"
            dateStr = formatter.string(from: days[i] as Date)
            
            arrOfDatesOfWeek.add(dateStr)
        }
//        print(days)
        
        print(arrOfDatesOfWeek)
        
//        let btnWeek: UIButton = stackViewTopBtn.viewWithTag(10) as! UIButton
//        btnWeek.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: .normal)
        
        arrDays = ["SUN", "MON", "TUE", "WED", "THR", "FRI", "SAT"]
        maxValue = 30.0
        minVAlue = 0.0
       
        //arrFasts = [15,22,10,8,14,29,6]
        countForXaxis = 7
        let xAxis = viewGraph.xAxis
        xAxis.labelPosition = .bottom
        xAxis.axisMinimum = 0
        xAxis.granularity = 1
        xAxis.valueFormatter = self
      
        NotificationCenter.default.addObserver(self, selector: #selector(ChartVC.TabChangedToChart), name: NSNotification.Name(rawValue: "TabChangedToChart"), object: nil)
      
        
    }
  
    
    override func viewWillDisappear(_ animated: Bool) {
        
         NotificationCenter.default.removeObserver(self)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
       let val = [".sv": "timestamp"]
        
        let serverTimeStamp = ServerValue.timestamp() as! [String:Any]
        
      
        viewGraph.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
         arrQadha = [0,0,0,0,0,0,0,0,0,0,0,0]  // move from did load aman
       
        stackViewTopBtn.subviews.forEach({element in
            (element as! UIButton).setTitleColor(.lightGray, for: .normal)
        })
        let btnWeek: UIButton = stackViewTopBtn.viewWithTag(11) as! UIButton
        btnWeek.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: .normal)
        
        
        
        getDataOfActivity()
        self.updateChartData()
    }
    
    //MARK: delegate method fires.
   @objc func TabChangedToChart() {
    viewGraph.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
    if !(InternetManager.isConnectedToNetwork()) {
        CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        return
    }
    
    }
    
    func generateDatesArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date] {
        var datesArray: [Date] =  [Date]()
        var startDate = startDate
        let calendar = Calendar.current
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd"
        
        while startDate <= endDate {
            datesArray.append(startDate)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
        }
        return datesArray
    }

    var arrForWeek = NSArray()
    var arrForCountWeek = ["WEEK1", "WEEK2", "WEEK3", "WEEK4", "WEEK5", "WEEK6", "WEEK7"]
    var daysNumber = Int()
    
    var arrOfDaysOne = NSMutableArray()
    var arrOfDaysTwo = NSMutableArray()
    var arrOfDaysThree = NSMutableArray()
    var arrOfDaysFour = NSMutableArray()
    var arrOfDaysFive = NSMutableArray()
    var arrOfDaysSix = NSMutableArray()
    
    var arrForDatesVAlues = NSMutableArray()
    var arrForMonthsVAlues = NSMutableArray()
  
    
    
    //MARK: top Action Here
    
    @IBAction func topBtnAction(_ sender: UIButton) {
      viewGraph.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
        stackViewTopBtn.subviews.forEach({element in
            (element as! UIButton).setTitleColor(.lightGray, for: .normal)
        })
        
        sender.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: .normal)
       
        switch sender.tag {
        case 10:
            arrDays = ["SUN", "MON", "TUE", "WED", "THR", "FRI", "SAT"]

          // aman
            self.arrQadha = [0,0,0,0,0,0,0]
            for i in 0..<self.arrOfDatesOfWeek.count{
                for j in 0..<self.arrValueDates.count{
                    if self.arrOfDatesOfWeek[i] as! String == self.arrValueDates[j] as! String {
                        let dict = self.arrValuesPrayCount[j] as! NSDictionary
                        self.arrQadha[i] = dict["prayCount"] as! Int
                        break
                    } else {
                        self.arrQadha[i] = 0
                    }
                }
            }
            print(self.arrQadha)
            
            
            
         
            countForXaxis = 7
            var ct:Int = 0
            for i in 0..<arrQadha.count {
                ct = ct + arrQadha[i]
            }
            lblTotalPrayed.text! = String(ct)
          
            if ct != 0 {
            maxValue = Double(ct + 10)
            minVAlue = 0.0
            }
            
            else {
                
                maxValue = 250.0
                minVAlue = 0.0
                
                
                
            }
            
          print(maxValue,minVAlue)
            self.updateChartData()
            
            break
        case 11:
            
          loadMonthData()
            
            break
        case 12:
          
            
            var total:Int = 0
            for i in 0..<arrValuesPrayCount.count {
                let dict = arrValuesPrayCount[i] as! NSDictionary
                total = total + Int(dict["prayCount"] as! Int)
            }
            
            arrDays = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN","JUL" ,"AUG" , "SEP", "OCT", "NOV", "DEC"]
           
            
            countForXaxis = 12

            arrForMonthsVAlues.removeAllObjects()
            for i in 0..<arrValueDates.count {
                let dateFormatterD = DateFormatter()
                dateFormatterD.dateFormat  = "dd MMM, yyyy"
                let dt = dateFormatterD.date(from: arrValueDates[i] as! String)
                
                let convertFor = DateFormatter()
                convertFor.dateFormat = "MM"
                let month = convertFor.string(from: dt as! Date)
                arrForMonthsVAlues.add(month)
            }
//            print(arrForMonthsVAlues)
            
            val1Months = 0
            val2Months = 0
            val3Months = 0
            val4Months = 0
            val5Months = 0
            val6Months = 0
            val7Months = 0
            val8Months = 0
            val9Months = 0
            val10Months = 0
            val11Months = 0
            val12Months = 0
            arrQadha.removeAll()
            
            for i in 0..<arrForMonthsVAlues.count {
                
                let dict = arrValuesPrayCount[i] as! NSDictionary
                
                if arrForMonthsVAlues[i] as! String == "01" {
                    val1Months = val1Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "02" {
                    val2Months = val2Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "03" {
                    val3Months = val3Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "04" {
                    val4Months = val4Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "05" {
                    val5Months = val5Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "06" {
                    val6Months = val6Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "07" {
                    val7Months = val7Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "08" {
                    val8Months = val8Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "09" {
                    val9Months = val9Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "10" {
                    val10Months = val10Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "11" {
                    val11Months = val11Months + Int(dict["prayCount"] as! Int)
                } else if arrForMonthsVAlues[i] as! String == "12" {
                    val12Months = val12Months + Int(dict["prayCount"] as! Int)
                }
            }
            arrQadha = [val1Months,val2Months,val3Months,val4Months,val5Months,val6Months,
                        val7Months,val8Months,val9Months,val10Months,val11Months,val12Months]
            print(self.arrQadha)
          
            
            
            
            var ct:Int = 0
            for i in 0..<arrQadha.count {
                ct = ct + arrQadha[i]
            }
            lblTotalPrayed.text! = String(ct)
            if ct != 0 {
                maxValue = Double(ct + 10)
                minVAlue = 0.0
            }
                
            else {
                
                maxValue = 250.0
                minVAlue = 0.0
                
            }
            
            
            
            self.updateChartData()
            
            break
        default:
            break
        }
    }
    
    var val1Months:Int = 0
    var val2Months:Int = 0
    var val3Months:Int = 0
    var val4Months:Int = 0
    var val5Months:Int = 0
    var val6Months:Int = 0
    
    var val7Months:Int = 0
    var val8Months:Int = 0
    var val9Months:Int = 0
    var val10Months:Int = 0
    var val11Months:Int = 0
    var val12Months:Int = 0
    
    var val1:Int = 0
    var val2:Int = 0
    var val3:Int = 0
    var val4:Int = 0
    var val5:Int = 0
    var val6:Int = 0
    // MARK: search For Value
    func searchForValue(numberOfWeeks:Int) {
//        print(arrForDatesVAlues)
//        print(arrOfDaysFour)
        val1 = 0
        val2 = 0
        val3 = 0
        val4 = 0
        val5 = 0
        val6 = 0
        
        for i in 0..<arrForDatesVAlues.count {
            
            let dict = arrValuesPrayCount[i] as! NSDictionary
            let prayCount = dict["prayCount"] as! Int
            
            if arrOfDaysOne.contains(arrForDatesVAlues[i]) {
               val1 = val1 + prayCount
            } else if arrOfDaysTwo.contains(arrForDatesVAlues[i] ) {
                val2 = val2 + prayCount
            } else if arrOfDaysThree.contains(arrForDatesVAlues[i] ) {
                val3 = val3 + prayCount
            } else if arrOfDaysFour.contains(arrForDatesVAlues[i] ) {
                val4 = val4 + prayCount
            } else if arrOfDaysFive.contains(arrForDatesVAlues[i] ) {
                val5 = val5 + prayCount
            } else if arrOfDaysSix.contains(arrForDatesVAlues[i] ) {
                val6 = val6 + prayCount
            }
        }
        arrQadha.removeAll()
        if numberOfWeeks == 4 {
            arrQadha = [val1 , val2 ,val3 , val4]
        } else if numberOfWeeks == 5 {
            arrQadha = [val1,val2,val3,val4,val5]
        } else if numberOfWeeks == 6 {
            arrQadha = [val1,val2,val3,val4,val5,val6]
        }
         print(self.arrQadha)
        var ct:Int = 0
        for i in 0..<arrQadha.count {
            ct = ct + arrQadha[i]
        }
        lblTotalPrayed.text! = String(ct)
        
        if ct != 0 {
            maxValue = Double(ct + 10)
            minVAlue = 0.0
        }
            
        else {
            
            maxValue = 250.0
            minVAlue = 0.0
            
            
            
        }
        
        
        
         self.updateChartData()
//        print("donee....")
    }
    
    
    
    func weeksInMonth(month: Int, forYear year: Int) -> Int? {
        if (month < 1 || month > 12) { return nil }
        
        let dateString = String(format: "%4d/%d/01", year, month)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        if let date = dateFormatter.date(from: dateString),
            let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        {
            calendar.firstWeekday = 1 // Sunday
            let weekRange = calendar.range(of: .weekOfMonth, in: .month, for: date)
            let weeksCount = weekRange.length
            return weeksCount
        }
        else
        {
            return nil
        }
    }
    
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
    
    var arrValueDates = NSArray()
    var arrValuesPrayCount = NSArray()
    
    func getDataOfActivity() {
     //   arrQadha = [0,0,0,0,0,0,0]
   
        if !(InternetManager.isConnectedToNetwork()) {
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            return
        }
        
        
        
//        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("activityLog").child(userID as! String).observeSingleEvent(of: .value, with: { (snapshot) in
          self.view.isUserInteractionEnabled = true
            let value = snapshot.value as? NSDictionary
//            print(value!)
            self.arrValueDates = (value!.allKeys) as NSArray
            self.arrValuesPrayCount = (value!.allValues) as NSArray
//            print(self.arrValueDates)
//            self.arrQadha = [0,0,0,0,0,0,0]
//            for i in 0..<self.arrOfDatesOfWeek.count{
//                for j in 0..<self.arrValueDates.count{
//                    if self.arrOfDatesOfWeek[i] as! String == self.arrValueDates[j] as! String {
//                        let dict = self.arrValuesPrayCount[j] as! NSDictionary
//                        self.arrQadha[i] = dict["prayCount"] as! Int
//                        break
//                    } else {
//                        self.arrQadha[i] = 0
//                    }
//                }
//            }
//            print(self.arrQadha)
            
//            var ct:Int = 0
//            for i in 0..<self.arrQadha.count {
//                ct = ct + self.arrQadha[i]
//            }
//            self.lblTotalPrayed.text! = String(ct)
//            //MARK: setting Max value
//            self.maxValue = Double(ct + 25)
//            self.minVAlue = 0.0
//
//            self.arrForWeek = self.arrQadha as NSArray
            DispatchQueue.main.async() {
//                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            self.loadMonthData()
           
        }) { (error) in
            self.view.isUserInteractionEnabled = true
            DispatchQueue.main.async() {
               // CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
    }
    
    func updateChartData() {
        if self.shouldHideData {
            viewGraph.data = nil
            return
        }
        self.setChartData()
    }
 
    
    //MARK: set chart Data Here
    func setChartData() {
        let data = CombinedChartData()
        data.lineData = generateLineData()
       
        viewGraph.chartDescription?.enabled = false
        viewGraph.dragEnabled = true
        // aman change to scale
        viewGraph.setScaleEnabled(true)
        viewGraph.pinchZoomEnabled = false
        viewGraph.xAxis.gridLineDashLengths = [0, 0]
        viewGraph.xAxis.gridLineDashPhase = 0
        
        let leftAxis = viewGraph.leftAxis
        leftAxis.removeAllLimitLines()
        leftAxis.axisMaximum = maxValue
        leftAxis.axisMinimum = minVAlue
        leftAxis.gridLineDashLengths = nil
        
        viewGraph.rightAxis.enabled = false
        viewGraph.xAxis.labelPosition = .bottom
        viewGraph.legend.form = .line
        //slidersValueChanged(nil)
        viewGraph.animate(xAxisDuration: 0.5)
    //    viewGraph.xAxis.axisMaximum = data.xMax + 0.25
        viewGraph.data = data


        
        
        
// aman
//    viewGraph.extraBottomOffset = 40
        viewGraph.leftAxis.drawGridLinesEnabled = false  // remove lines grid
        viewGraph.xAxis.drawGridLinesEnabled = false
        
//        if maxValue == 250.0 {
//           viewGraph.leftAxis.axisMinimum = -3 // maximum vertical value
//
//        }
//        else {
//
//         viewGraph.leftAxis.axisMinimum = 5 // maximum vertical value
//
//        }
      
       // viewGraph.rightAxis.axisMinimum = -20
        viewGraph.data?.highlightEnabled = false
    }
    
    func generateLineData() -> LineChartData {
        
        //let arr1 = [22,42,50,67,10]
        
//        print(countForXaxis)
        print("arrQadha\(arrQadha)")
        let values = (0..<countForXaxis).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: Double(i), y: Double(arrQadha[i]))
        }
//        //let arr2 = [10,15,74,25,60]
//        let values2 = (0..<countForXaxis).map { (i) -> ChartDataEntry in
//            return ChartDataEntry(x: Double(i), y: Double(arrFasts[i]))
//        }
        let set1 = LineChartDataSet(values: values, label: nil)
        set1.drawIconsEnabled = false
        set1.lineDashLengths = [0, 0]
        set1.highlightLineDashLengths = [0, 0]
        set1.setColor(UIColor.init(hexString: "#66E4BF"))
        
        set1.setCircleColor(.clear)
        set1.lineWidth = 1.5
        set1.drawCircleHoleEnabled = false
        set1.valueFont = .systemFont(ofSize: 9 )
        set1.formLineDashLengths = [0, 0]
        set1.formLineWidth = 1
        set1.formSize = 0
        set1.circleRadius = 0.0
        set1.mode = .cubicBezier
        set1.valueTextColor = UIColor.clear
        
        
        let gradientColors = [ChartColorTemplates.colorFromString("#FFFFFF").cgColor, ChartColorTemplates.colorFromString("#B8E986").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        set1.fillAlpha = 0.7
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
//       viewGraph.leftAxis.axisMinimum = -80.0
//        viewGraph.rightAxis.axisMinimum = -80.0
        
//        let set2 = LineChartDataSet(values: values2, label: nil)
//        set2.drawIconsEnabled = false
//        set2.lineDashLengths = [0, 0]
//        set2.highlightLineDashLengths = [0, 0]
//        set2.setColor(UIColor.init(hexString: "#CEF2C4"))
//        set2.setCircleColor(.clear)
//        set2.lineWidth = 1.5
//        set2.circleRadius = 0.0
//        set2.drawCircleHoleEnabled = false
//        set2.valueFont = .systemFont(ofSize: 9)
//        set2.formLineDashLengths = [0, 0]
//        set2.formLineWidth = 1
//        set2.formSize = 0
//        set2.valueTextColor = UIColor.clear
//        set2.mode = .cubicBezier
//        let gradientColors2 = [ChartColorTemplates.colorFromString("#CEF2C4").cgColor, ChartColorTemplates.colorFromString("#CEF2C4").cgColor]
//        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
//        set2.fillAlpha = 0.7
//        set2.fill = Fill(linearGradient: gradient2, angle: 90) //.linearGradient(gradient, angle: 90)
//        set2.drawFilledEnabled = true
        
        //                let data = LineChartData(dataSets: [set1 , set2])
        //                viewGraph.data = data
        
        return LineChartData(dataSets: [set1])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Load month data
    func loadMonthData() {
        
        daysNumber = 0
        let formatter = DateFormatter()
        let formatter1 = DateFormatter()
        var monthStr = String()
        var yearStr = String()
        formatter.dateFormat = "MM"
        formatter1.dateFormat = "yyyy"
        monthStr = formatter.string(from: self.curentDate as Date)
        yearStr = formatter1.string(from: self.curentDate as Date)
        
        let dateComponents = DateComponents(year: Int(yearStr), month: Int(monthStr))
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
//        print(numDays) // 31
        
        daysNumber = numDays
        
        let weekArr = NSMutableArray()
        
        let noWeeks = weeksInMonth(month: Int(monthStr)!, forYear: Int(yearStr)!)
//        print(noWeeks!)
        
        countForXaxis = noWeeks!
//        maxValue = 250.0
//        minVAlue = 0.0
        weekArr.removeAllObjects()
        for i in 0..<noWeeks! {
            weekArr.add(arrForCountWeek[i])
        }
        arrDays = weekArr as NSArray
        
        let dateInWeek = Date()//7th June 2017
        let dayOfWeek = calendar.component(.weekday, from: dateInWeek)
        let weekdays = calendar.range(of: .weekday, in: .weekOfMonth, for: dateInWeek)!
        let days = (weekdays.lowerBound ..< weekdays.upperBound)
            .flatMap { calendar.date(byAdding: .day, value: $0 - dayOfWeek, to: dateInWeek) }
        
//        print(days)
        
        let datea = NSDate()
        let dateFormatterM = DateFormatter()
        let dateFormatterY = DateFormatter()
        dateFormatterM.dateFormat  = "MMM"
        dateFormatterY.dateFormat  = "YYYY"
        
        let mon = dateFormatterM.string(from: datea as Date)
        let yr = dateFormatterY.string(from: datea as Date)
        let firstDAte = "1-\(mon)-\(yr)" as! String
        
        let dateFormatterD = DateFormatter()
        dateFormatterD.dateFormat  = "dd-MMM-yyyy"
        let dt = dateFormatterD.date(from: firstDAte)
        
        let convertFor = DateFormatter()
        convertFor.dateFormat = "EEEE"
        let dayInWeek = convertFor.string(from: dt as! Date)//"Sunday"
//        print(dayInWeek)
        
        arrOfDaysOne.removeAllObjects()
        arrOfDaysTwo.removeAllObjects()
        arrOfDaysThree.removeAllObjects()
        arrOfDaysFour.removeAllObjects()
        arrOfDaysFive.removeAllObjects()
        arrOfDaysSix.removeAllObjects()
        arrForDatesVAlues.removeAllObjects()
        
        for i in 0..<arrValueDates.count {
            let dateFormatterD = DateFormatter()
            dateFormatterD.dateFormat  = "dd MMM, yyyy"
            let dt = dateFormatterD.date(from: arrValueDates[i] as! String)
            
            let convertFor = DateFormatter()
            convertFor.dateFormat = "dd"
            let dayy = convertFor.string(from: dt as! Date)//"Sunday"
            arrForDatesVAlues.add(dayy)
        }
//        print(arrForDatesVAlues)
        if dayInWeek == "Sunday" {
            arrOfDaysOne = ["01","02","03","04","05","06","07"]
            arrOfDaysTwo = ["08","09","10","11","12","13","14"]
            arrOfDaysThree = ["15","16","17","18","19","20","21"]
            arrOfDaysFour = ["22","23","24","25","26","27","28"]
            arrOfDaysFive = ["29","30","31"]
        } else if dayInWeek == "Monday" {
            arrOfDaysOne = ["01","02","03","04","05","06"]
            arrOfDaysTwo = ["07","08","09","10","11","12","13"]
            arrOfDaysThree = ["14","15","16","17","18","19","20"]
            arrOfDaysFour = ["21","22","23","24","25","26","27"]
            arrOfDaysFive = ["28","29","30","31"]
        } else if dayInWeek == "Tuesday" {
            arrOfDaysOne = ["01","02","03","04","05"]
            arrOfDaysTwo = ["06","07","08","09","10","11","12"]
            arrOfDaysThree = ["13","14","15","16","17","18","19"]
            arrOfDaysFour = ["20","21","22","23","24","25","26"]
            arrOfDaysFive = ["27","28","29","30","31"]
        } else if dayInWeek == "Wednesday" {
            arrOfDaysOne = ["01","02","03","04"]
            arrOfDaysTwo = ["05","06","07","08","09","10","11"]
            arrOfDaysThree = ["12","13","14","15","16","17","18"]
            arrOfDaysFour = ["19","20","21","22","23","24","25"]
            arrOfDaysFive = ["26","27","28","29","30","31"]
        } else if dayInWeek == "Thrusday" {
            arrOfDaysOne = ["01","02","03"]
            arrOfDaysTwo = ["04","05","06","07","08","09","10"]
            arrOfDaysThree = ["11","12","13","14","15","16","17"]
            arrOfDaysFour = ["18","19","20","21","22","23","24"]
            arrOfDaysFive = ["25","26","27","28","29","30","31"]
        } else if dayInWeek == "Friday" {
            arrOfDaysOne = ["01","02"]
            arrOfDaysTwo = ["03","04","05","06","07","08","09"]
            arrOfDaysThree = ["10","11","12","13","14","15","16"]
            arrOfDaysFour = ["17","18","19","20","21","22","23"]
            arrOfDaysFive = ["24","25","26","27","28","29","30"]
            arrOfDaysSix = ["31"]
        } else if dayInWeek == "Saturday" {
            arrOfDaysOne = ["01"]
            arrOfDaysTwo = ["02","03","04","05","06","07","08"]
            arrOfDaysThree = ["09","10","11","12","13","14","15"]
            arrOfDaysFour = ["16","17","18","19","20","21","22"]
            arrOfDaysFive = ["23","24","25","26","27","28","29"]
            arrOfDaysSix = ["30","31"]
        }
        searchForValue(numberOfWeeks:noWeeks!)
        var total:Int = 0
        for i in 0..<arrValuesPrayCount.count {
            let dict = arrValuesPrayCount[i] as! NSDictionary
            total = total + Int(dict["prayCount"] as! Int)
        }
        
        
    }
    
    
}


extension ChartVC: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        print(arrDays[Int(value) % arrDays.count] as! String)
        return arrDays[Int(value) % arrDays.count] as! String
    }
}

extension Date {
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
}
