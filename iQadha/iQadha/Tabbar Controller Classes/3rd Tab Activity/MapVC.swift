


//
//  MapVC.swift
//  iQadha
//
//  Created by NetSet on 4/6/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import MapKit
import Cluster
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseDatabase

class MapVC: UIViewController ,CLLocationManagerDelegate{

    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var lblReadingCount: UILabel!
    let manager = ClusterManager()
    let locationManager = CLLocationManager()
    var ref: DatabaseReference!
    var locationServiceEnabled = false
   var zoomCount = 0
    var handle: UInt!
    
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          ref = Database.database().reference()
           NotificationCenter.default.addObserver(self, selector: #selector(MapVC.TabChangedToMap), name: NSNotification.Name(rawValue: "TabChangedToMap"), object: nil)
//        mapView.isZoomEnabled = false

       // self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.startUpdatingLocation()
            
        }
    }
    //MARK:-  view will Appear
    override func viewWillAppear(_ animated: Bool) {
  
    
        
 observeChilds()
        locationManager.delegate = self

        switch CLLocationManager.authorizationStatus()  {
            
        case .notDetermined:
            
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            locationManager.startUpdatingLocation()
            break
        case .denied:
            let alertController = UIAlertController(title: "iQadha", message: "GPS location service is disabled on your device. Go to device settings and enable location to see charts, activity log and live view.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                let url = URL(string: UIApplicationOpenSettingsURLString)
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                print("Cancel Pressed")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
            //            self.alertMessageWithAction(title: "iQadha", message: "Please enable location", viewcontroller: self, action: move_to_settingToEnableLocation)
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        }
//
//        if locationManager.location != nil
//        {
//
//        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse) {
//            if locationManager.location != nil
//            {
//
//                updateLatLong(value: "\(lat),\(long)", param: "location")
////                currentlocation = locationManager.location
////                callToGetProfileApi()
//            }
//        }
//        else {
//            let alertController = UIAlertController(title: "iQadha", message: "Please enable location.", preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
//                UIAlertAction in
//                let url = URL(string: UIApplicationOpenSettingsURLString)
//                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
//            }
//            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
//                UIAlertAction in
//                print("Cancel Pressed")
//            }
//            alertController.addAction(okAction)
//            alertController.addAction(cancelAction)
//            self.present(alertController, animated: true, completion: nil)
//
////            self.alertMessageWithAction(title: "iQadha", message: "Please enable location", viewcontroller: self, action: move_to_settingToEnableLocation)
//
//        }
//        }
   }
    
    
    //MARK:- remove observers Here
    override func viewWillDisappear(_ animated: Bool) {
    NotificationCenter.default.removeObserver(self)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        ref.removeObserver(withHandle: handle)
       
    }
    
    /*
     func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
     switch status {
     case .notDetermined:
     // If status has not yet been determied, ask for authorization
     
     manager.requestWhenInUseAuthorization()
     break
     case .authorizedWhenInUse:
     // If authorized when in use
     manager.startUpdatingLocation()
     break
     case .authorizedAlways:
     // If always authorized
     manager.startUpdatingLocation()
     break
     case .restricted:
     
     // If restricted by e.g. parental controls. User can't enable Location Services
     break
     case .denied:
     let alertController = UIAlertController(title: "iQadha", message: "Please enable location.", preferredStyle: .alert)
     let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
     UIAlertAction in
     let url = URL(string: UIApplicationOpenSettingsURLString)
     UIApplication.shared.open(url!, options: [:], completionHandler: nil)
     }
     let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
     UIAlertAction in
     print("Cancel Pressed")
     }
     alertController.addAction(okAction)
     alertController.addAction(cancelAction)
     self.present(alertController, animated: true, completion: nil)
     
     //            self.alertMessageWithAction(title: "iQadha", message: "Please enable location", viewcontroller: self, action: move_to_settingToEnableLocation)
     // If user denied your app access to Location Services, but can grant access from Settings.app
     break
     default:
     break
       }
     }
 */
    
    // MARK: - Enable location from setting
    func move_to_settingToEnableLocation()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    var lat:Double = 0.0
    var long:Double = 0.0
    
    var count:Int = 0
    
    func makeCluster(lat:Double , long:Double , arrCount:Int) {

      
      
        
        
      //  self.manager.removeAll()
       // self.mapView.removeAnnotations(self.mapView.annotations)
        
  //      print("..............\(arrCount)")
        count = count + 1
        manager.cellSize = nil
        manager.maxZoomLevel = 15
        manager.minCountForClustering = 1
        manager.shouldRemoveInvisibleAnnotations = false
        manager.clusterPosition = .nearCenter
        
        let center = CLLocationCoordinate2D(latitude: lat, longitude: long) // region center
        let delta = 0.001
//        let annotations: [Annotation] = (0..<arrCount).map { i in
            let annotation = Annotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: center.latitude + drand48() * delta - delta / 2, longitude: center.longitude + drand48() * delta - delta / 2)
            let color = UIColor(red: 114/255, green: 176/255, blue: 146/255, alpha: 1)
            annotation.style = .color(color, radius: 25)
//            return annotation
//        }
        manager.add(annotation)
       // mapView.animate(to: GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 16.0))
        if count == 1 {
            mapView.region = .init(center: center, span: .init(latitudeDelta: delta, longitudeDelta: delta))
        } else {
            
        }
        
    }
    
    private let regionRadius: CLLocationDistance = 200
    
    func centreMap(on location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        
        
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationServiceEnabled = true
       
        
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        long = (locValue.longitude)
        let initialLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
       // centreMap(on: initialLocation)
        
      
     
        updateLatLong(value: "\(locValue.latitude) , \(locValue.longitude)", param: "location")
    }

    
    func updateLatLong(value:String , param:String) {
        if let ud = UserDefaults.standard.value(forKey: "AuthId") as? String {
            if ud == nil {
                
            } else {
                arrOfLocations = []
                let valuesForLatLong = ["\(param)": "\(value)" ]
//                print(valuesForLatLong)
                let userID = UserDefaults.standard.value(forKey: "AuthId")!
                
               
                // update time stamp
                ref.child("users").child(userID as! String).updateChildValues(["online":true])
                 ref.child("users").child(userID as! String).updateChildValues(["onlineTime":ServerValue.timestamp()])
                
                //self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName)
                
                self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForLatLong) { (error, ref) in
                    if error == nil {
//                        self.manager.reload(self.mapView, visibleMapRect: self.mapView.visibleMapRect)
//                   self.arrOfLocations.removeAllObjects()
                      //  self.fetchAllUsersLatLong()
                        
                    } else{
                        CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                        print("error \(String(describing: error))")
                    }
                }
            }
        }
    }
    
    var arrOfLocations = NSMutableArray()
    
    func getTimeDifference(timeStamp:Int,dict:NSDictionary) -> Bool {

//
//        let date = NSDate(timeIntervalSince1970: TimeInterval(timeStamp / 1000))
//        let dateFormatter = DateFormatter()
////        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
//      //  dateFormatter.timeZone = TimeZone.current
//        let dateString = dateFormatter.string(from: date as Date)
        
 
        let date = NSDate(timeIntervalSince1970: TimeInterval(timeStamp / 1000))
        
        let reducedTime = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)?.date(byAdding: .minute, value: -15, to: date as Date, options: NSCalendar.Options())
        
        
        
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        let date1String = dayTimePeriodFormatter.string(from: reducedTime as! Date)
        
     
      
        let date1 = Date()
//        print(date1.timeIntervalSince1970)
//        print( date1.minuates(from: reducedTime!))
        
//        print( date1.minuates(from: date as Date))
        
        
        let date2 = Date(timeIntervalSince1970: TimeInterval(timeStamp)) // 3:30
       
//        print(dict)
        
       
        
        
        let diff = Int(date2.timeIntervalSince1970 - date1.timeIntervalSince1970)
        
//        print(date2.timeIntervalSince1970 )
//          print(date1.timeIntervalSince1970 )
        
    
        let hours = diff / 3600
        let minutes = (diff - hours * 3600) / 60
        
//     //  print("Minute: \(minutes)")
//        if minutes <= 10 {
//            return true
//        } else {
//            return false
//        }
        
        if date1.minuates(from: date as Date) <= 10 {
            return true
            print(dict)
            
        }
        else {
          return false
           
        }
        
    }
 
    
    //MARK:__ Fetch All Users
    func fetchAllUsersLatLong() {
//   print("fetch all users is working finew nowaskdaskjdhasjkhdkjashkdhaskdhaskjdkasdkajshdkasdhaksjdhka................................")
        print("empty array",arrOfLocations.count)
    
      self.arrOfLocations.removeAllObjects()
      
     
        let reff = self.ref.child("users")
        reff.queryOrdered(byChild: "users").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    self.arrOfLocations.removeAllObjects()


                    print("snap shot cout",snapshot.childrenCount)
                    for child in (snapshot.value as! NSDictionary) {
                        let dict = child.value as! NSDictionary
                        let onlineStatus = dict["online"] as! Bool
                        if onlineStatus == true {
               
                            let chkkOnlineTime = self.getTimeDifference(timeStamp: dict["onlineTime"] as! Int, dict: dict)
                        if let dc = dict["userProfile"] as?  NSDictionary {
                        let dictUser = dict["userProfile"] as!  NSDictionary
                       
                        let serverId = dictUser["userid"] as! String
                            
                       let location = dictUser["location"] as! String
                       let trimLocation = location.split(separator: ",")
                      let latString = trimLocation[0].trimmingCharacters(in: .whitespaces)
//                    && Double(latString) != 0.0
                        if chkkOnlineTime == true &&  Double(latString) != 0.0  {
                            self.arrOfLocations.add(dictUser["location"] as! String)
//                             self.lblReadingCount.text! = String(self.arrOfLocations.count)
//                            self.makeCluster(lat: self.lat , long: self.long , arrCount:self.arrOfLocations.count)

                            
                        }
                }
                        }
              }
                    
                    self.lblReadingCount.text! = String(self.arrOfLocations.count)
                    
 
                    print("count is  ", self.arrOfLocations.count)
                    
                    var latofLastAnnotation:Double! = 0.0
                    var longofLastAnnotation:Double!
                    
                       self.manager.removeAll()
                    
                    for i in 0..<self.arrOfLocations.count {
                let serverString = self.arrOfLocations[i] as! String
                 let splitString = serverString.split(separator: ",")
                let latString = splitString[0].trimmingCharacters(in: .whitespaces)
                        
                let longString = splitString[1].trimmingCharacters(in: .whitespaces)
             latofLastAnnotation = Double(latString)
            longofLastAnnotation = Double(longString)
            self.makeCluster(lat: Double(latString)!, long: Double(longString)!, arrCount: self.arrOfLocations.count)
                        
                        if i == self.arrOfLocations.count - 1 && self.zoomCount == 0 && Double(latString) != 0.0 {
                            self.mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2DMake(latofLastAnnotation, longofLastAnnotation), span: MKCoordinateSpanMake(0.1, 0.1 )), animated: true)
                                                    self.zoomCount = self.zoomCount + 1
                            
                        }
                        
                    }
                 
//                    if  (self.zoomCount == 0 && latofLastAnnotation != 0.0 ) {
//                      self.mapView.setRegion(MKCoordinateRegion(center: CLLocationCoordinate2DMake(latofLastAnnotation, longofLastAnnotation), span: MKCoordinateSpanMake(0.0144927536, 0.0144927536)), animated: true)
//                        self.zoomCount = self.zoomCount + 1
//
//                    }
                 
                  self.manager.reload(self.mapView, visibleMapRect: self.mapView.visibleMapRect)
                 
            } else {
                print("no snapshot")
            }
            
            })
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Get All Chaild:--
    func observeChilds() {
    
    
        
        if !(InternetManager.isConnectedToNetwork()) {
             CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            return
        }
      
        
        
        
        if self.arrOfLocations.count > 0{
            self.arrOfLocations.removeAllObjects()
            
        }
        
        handle =  ref.child("users").observe(.childChanged, with: { (shot) in
      
            if self.arrOfLocations.count > 0{
               self.arrOfLocations.removeAllObjects()
                
            }
            
           self.fetchAllUsersLatLong()
                
            
            })
        }
   
    
    
    @objc func TabChangedToMap() {
        
        if !(InternetManager.isConnectedToNetwork()) {
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            return
        }
        
        
        
    }
    
}
extension MapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        

        if let annotation = annotation as? ClusterAnnotation {
            guard let style = annotation.style else { return nil }
            let identifier = "Cluster"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if let view = view as? BorderedClusterAnnotationView {
                view.annotation = annotation
                view.configure(with: style)
            } else {
                view = BorderedClusterAnnotationView(annotation: annotation, reuseIdentifier: identifier, style: style, borderColor: .white)
            }
            return view
        }
        else {
            let vw = MKPinAnnotationView()
            vw.isHidden = true
            return vw
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {

        
        if zoomCount != 0 {
        print("Zoom: \(mapView.getZoomLevel())")
        if mapView.getZoomLevel() > 10 {
            mapView.setCenter(coordinate: mapView.centerCoordinate, zoomLevel: 10, animated: true)
        }
        else {
      manager.reload(mapView, visibleMapRect: mapView.visibleMapRect)
            
        }
        
        
        
//        let zoomWidth = mapView.visibleMapRect.size.width
//        let zoomFactor = Int(log2(zoomWidth)) - 9
//
//
//
//        print(zoomFactor)
//
//
//        var longitudeDelta: CLLocationDegrees = mapView.region.span.longitudeDelta
//        var mapWidthInPixels: CGFloat = mapView.bounds.size.width
//        var zoomScale = Double(longitudeDelta * 85445659.44705395 * .pi / (180.0 * Double(mapWidthInPixels)))
//        var zoomer: Double = 20 - log2(zoomScale)
//        if zoomer < 0 {
//            zoomer = 0
//        }
//        //  zoomer = round(zoomer);
//       print(zoomer)
//
////        if zoomFactor > 5 {
//        manager.reload(mapView, visibleMapRect: mapView.visibleMapRect)
////        }
////        else {
//
////        mapView.setCenter(mapView.centerCoordinate, animated: true)
//
//     //   mapView.setRegion(mapView.region.center, animated: true)
      }
      
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        guard let annotation = view.annotation else { return }
//
//        if let cluster = annotation as? ClusterAnnotation {
//            var zoomRect = MKMapRectNull
//            for annotation in cluster.annotations {
//                let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
//                let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0)
//                if MKMapRectIsNull(zoomRect) {
//                    zoomRect = pointRect
//                } else {
//                    zoomRect = MKMapRectUnion(zoomRect, pointRect)
//                }
//            }
//            mapView.setVisibleMapRect(zoomRect, animated: true)
//        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        views.forEach { $0.alpha = 0 }
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
            views.forEach { $0.alpha = 1 }
        }, completion: nil)
        

    }
    

 
    
}

let MERCATOR_OFFSET: Double = 268435456
let MERCATOR_RADIUS: Double = 85445659.44705395
extension MKMapView {
    
    func getZoomLevel() -> Double {
        
        let reg = self.region
        let span = reg.span
        let centerCoordinate = reg.center
        
        // Get the left and right most lonitudes
        let leftLongitude = centerCoordinate.longitude - (span.longitudeDelta / 2)
        let rightLongitude = centerCoordinate.longitude + (span.longitudeDelta / 2)
        let mapSizeInPixels = self.bounds.size
        
        // Get the left and right side of the screen in fully zoomed-in pixels
        let leftPixel = self.longitudeToPixelSpaceX(longitude: leftLongitude)
        let rightPixel = self.longitudeToPixelSpaceX(longitude: rightLongitude)
        let pixelDelta = abs(rightPixel - leftPixel)
        
        let zoomScale = Double(mapSizeInPixels.width) / pixelDelta
        let zoomExponent = log2(zoomScale)
        let zoomLevel = zoomExponent + 20
        
        return zoomLevel
    }
    
    func setCenter(coordinate: CLLocationCoordinate2D, zoomLevel: Int, animated: Bool) {
        
        let zoom = min(zoomLevel, 28)
        
        let span = self.coordinateSpan(centerCoordinate: coordinate, zoomLevel: zoom)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        
        self.setRegion(region, animated: true)
    }
    
    // MARK: - Private func
    
    private func coordinateSpan(centerCoordinate: CLLocationCoordinate2D, zoomLevel: Int) -> MKCoordinateSpan {
        
        // Convert center coordiate to pixel space
        let centerPixelX = self.longitudeToPixelSpaceX(longitude: centerCoordinate.longitude)
        let centerPixelY = self.latitudeToPixelSpaceY(latitude: centerCoordinate.latitude)
        
        // Determine the scale value from the zoom level
        let zoomExponent = 20 - zoomLevel
        let zoomScale = NSDecimalNumber(decimal: pow(2, zoomExponent)).doubleValue
        
        // Scale the map’s size in pixel space
        let mapSizeInPixels = self.bounds.size
        let scaledMapWidth = Double(mapSizeInPixels.width) * zoomScale
        let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale
        
        // Figure out the position of the top-left pixel
        let topLeftPixelX = centerPixelX - (scaledMapWidth / 2)
        let topLeftPixelY = centerPixelY - (scaledMapHeight / 2)
        
        // Find delta between left and right longitudes
        let minLng: CLLocationDegrees = self.pixelSpaceXToLongitude(pixelX: topLeftPixelX)
        let maxLng: CLLocationDegrees = self.pixelSpaceXToLongitude(pixelX: topLeftPixelX + scaledMapWidth)
        let longitudeDelta: CLLocationDegrees = maxLng - minLng
        
        // Find delta between top and bottom latitudes
        let minLat: CLLocationDegrees = self.pixelSpaceYToLatitude(pixelY: topLeftPixelY)
        let maxLat: CLLocationDegrees = self.pixelSpaceYToLatitude(pixelY: topLeftPixelY + scaledMapHeight)
        let latitudeDelta: CLLocationDegrees = -1 * (maxLat - minLat)
        
        return MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
    }
    
    private func longitudeToPixelSpaceX(longitude: Double) -> Double {
        return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0)
    }
    
    private func latitudeToPixelSpaceY(latitude: Double) -> Double {
        if latitude == 90.0 {
            return 0
        } else if latitude == -90.0 {
            return MERCATOR_OFFSET * 2
        } else {
            return round(MERCATOR_OFFSET - MERCATOR_RADIUS * Double(logf((1 + sinf(Float(latitude * M_PI) / 180.0)) / (1 - sinf(Float(latitude * M_PI) / 180.0))) / 2.0))
        }
    }
    
    private func pixelSpaceXToLongitude(pixelX: Double) -> Double {
        return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI
    }
    
    
    private func pixelSpaceYToLatitude(pixelY: Double) -> Double {
        return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI
    }
}




class BorderedClusterAnnotationView: ClusterAnnotationView {
    let borderColor: UIColor
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, style: ClusterAnnotationStyle, borderColor: UIColor) {
        self.borderColor = borderColor
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier, style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure(with style: ClusterAnnotationStyle) {
        super.configure(with: style)
        
        switch style {
        case .image:
            layer.borderWidth = 0
        case .color:
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 2
        }
    }
    
    
    
}

extension Date {
    /// Returns the amount of years from another date
    func minuates(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
}
}

