//
//  ContactVC.swift
//  iQadha
//
//  Created by netset on 4/3/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit

class ContactVC: UIViewController {

    @IBOutlet var attString: UILabel!
    var attributedString = NSMutableAttributedString()
    override func viewDidLoad() {
        super.viewDidLoad()
        let startIndex1 = 49
        let endIndex1 = 65
        let size = (endIndex1 - startIndex1)
        let range = NSRange(location:startIndex1,length:size)
        attributedString = NSMutableAttributedString(string: attString.text!, attributes: [NSAttributedStringKey.font:UIFont(name: "ProximaNova-Semibold", size: 16.0)!])
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 114/255, green: 177/255, blue: 146/255, alpha: 1.0), range: range)
        attString.attributedText = attributedString
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ContactVC.tapFunction))
        attString.isUserInteractionEnabled = true
        attString.addGestureRecognizer(tap)
     
        
    }

    @objc func tapFunction(gesture:UITapGestureRecognizer) {
        print("tap working")
        let startIndex1 = 49
        let endIndex1 = 63
        let size = (endIndex1 - startIndex1)
        let range = NSRange(location:startIndex1,length:size)
        if gesture.didTapAttributedTextInLabel(label: attString, inRange: range) {
            print("Tapped targetRange1")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.automaticallyAdjustsScrollViewInsets=false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint( x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                      y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

