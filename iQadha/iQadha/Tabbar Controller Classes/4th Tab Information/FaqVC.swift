//
//  FaqVC.swift
//  iQadha
//
//  Created by netset on 4/3/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class FaqVC: UIViewController {

    @IBOutlet var tblVw: UITableView!
    
    @IBOutlet var lblTitle: UILabel!
    var arraySelctedHeader = NSMutableArray()
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        arrFaq = []
        self.tblVw.sectionHeaderHeight = UITableViewAutomaticDimension;
        self.tblVw.estimatedSectionHeaderHeight = 50;
        getFaq()
    }

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
   var arrFaq = NSMutableArray()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getFaq() {
        
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        
        ref.child("information").child("language").child("english").child("pageContent").child("faq").observeSingleEvent(of: .value, with: { (snapshot) in
            
//            for rest in snapshot.children.allObjects as! [DataSnapshot] {
//                //let dic = rest.value as! [String: Any]
//                if let str = rest.value as? String {
//                    print(str)
//                }
//                if let dict = rest.value as? [String: Any] {
//                   print(dict)
//                }
//
//               // self.arrCountOfPrayers.add(dic)
//            }
            
            let value = snapshot.value as! NSDictionary
            print(value)
            self.lblTitle.text! = value["faqTitle"] as! String
            self.arrFaq = value["questionnaire"] as! NSArray as! NSMutableArray
            self.arrFaq.removeObject(at: 0)
            self.tblVw.dataSource = self
            self.tblVw.delegate = self
            self.tblVw.reloadData()

            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            
            
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
    }

}

extension FaqVC : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellForInformationRowFaq =  tableView.dequeueReusableCell(withIdentifier: "cellRow") as! cellForInformationRowFaq
        
        let dict = arrFaq[indexPath.section] as! NSDictionary
        let str = dict["answer"] as! String
       let str1 = str.replacingOccurrences(of: "_n", with: "\n")
        cell.labelForAnswer.text! = str1 as! String

        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
//
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arraySelctedHeader.count>0{
            if arraySelctedHeader.contains(section){
                return 1
            }
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrFaq.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : cellForInformationFaq =  tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! cellForInformationFaq
        let dict = arrFaq[section] as! NSDictionary
        cell.labelForQuestion.text! = dict["question"] as! String
        
        cell.btnForSelctedHeader.addTarget(self, action: #selector(tapOnHeader), for: .touchUpInside)
        cell.btnForSelctedHeader.tag = section+1

        if arraySelctedHeader.contains(section) {
        //   cell.bottomView.isHidden = true
            cell.imgViewDropdown.image = #imageLiteral(resourceName: "upArrow")
            
            cell.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
            cell.bottomView.isHidden = true
            
        } else {
           cell.bottomView.isHidden = false
            cell.imgViewDropdown.image = #imageLiteral(resourceName: "downArrow")
        }
        
        return cell
    }
    
    @objc func tapOnHeader(_ sender: UIButton) {
        print(sender.tag-1)
        if arraySelctedHeader.contains(sender.tag-1) {
            arraySelctedHeader.remove(sender.tag-1)
        } else {
             arraySelctedHeader.removeAllObjects()
            arraySelctedHeader.add(sender.tag-1)
        }
        tblVw.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

class cellForInformationFaq: UITableViewCell {
    @IBOutlet weak var labelForQuestion: UILabel!
    @IBOutlet weak var imgViewDropdown: UIImageView!
    @IBOutlet weak var btnForSelctedHeader: UIButton!
    @IBOutlet weak var bottomView: UIView!
}

class cellForInformationRowFaq: UITableViewCell {
    @IBOutlet weak var labelForAnswer: UILabel!
}
