//
//  InformVC.swift
//  iQadha
//
//  Created by netset on 4/3/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class InformVC: UIViewController {
    
    @IBOutlet weak var txtViewAboutUs: UITextView!
    @IBOutlet var lblTitle: UILabel!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
         getInfo()
    }
 
    func getInfo() {
        
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        ref.child("information").child("language").child("english").child("pageContent").child("aboutUs").observeSingleEvent(of: .value, with: { (snapshot) in

            let value = snapshot.value as! NSDictionary
            print(value)
            
            let str = value["aboutUsContent"] as! String
            let str1 = str.replacingOccurrences(of: "_n ", with: "\n")
            let str2 = str1.replacingOccurrences(of: "_n", with: "\n")

            self.txtViewAboutUs.text! = str2 as! String
//            self.txtViewAboutUs.textAlignment = .left
            self.lblTitle.text! = value["aboutUsTitle"] as! String
            
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
