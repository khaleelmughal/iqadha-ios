//
//  InformationVC.swift
//  iQadha
//
//  Created by Ranjana Prashar on 3/8/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit

class InformationVC: UIViewController {
    var closeHandler: (() -> Void)?
    @IBOutlet weak var tableViewInfo: UITableView!
    var arraySelctedHeader = NSMutableArray()
    
    class func instance() -> InformationVC {
        var storyboard = UIStoryboard()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InformationID") as! InformationVC
    }
    
//    @IBOutlet weak var cntctLbl: UILabel!
//    @IBOutlet weak var faqLbl: UILabel!
//    @IBOutlet weak var aboutLbl: UILabel!
//    @IBOutlet weak var aboutView: UIView!
//    @IBOutlet weak var contactView: UIView!
//    @IBOutlet weak var faqView: UIView!
//    @IBOutlet weak var contactBtn: UIButton!
//    @IBOutlet weak var faqBtn: UIButton!
//    @IBOutlet weak var aboutBtn: UIButton!
//
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.view .addSubview(aboutView)
       // self.view .addSubview(contactView)
       // aboutView.isHidden = false
       // contactView .isHidden = true
       // aboutLbl.isHidden = false
       // faqLbl.isHidden = false
        //cntctLbl.isHidden = false
        navigationItem.title = "INFORMATION"
       // faqLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
      // aboutBtn.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: UIControlState.normal)
      //    faqBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
       //   contactBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
     //   aboutLbl.backgroundColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0)
     //   cntctLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
        UIApplication.shared.isStatusBarHidden = true
        
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
//    @IBAction func aboutBtn(_ sender: Any) {
//        aboutView.isHidden = false
//        faqLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        aboutLbl.backgroundColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0)
//        cntctLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        aboutBtn.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: UIControlState.normal)
//        faqBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//        contactBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//         contactView .isHidden = true
////        aboutLbl.isHidden = false
////        faqLbl.isHidden = true
////        cntctLbl.isHidden = true
//    }
//    @IBAction func faqBtn(_ sender: Any) {
//         aboutView.isHidden = true
//        faqLbl.backgroundColor = UIColor .red
//        aboutLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        faqLbl.backgroundColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0)
//        cntctLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        faqBtn.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: UIControlState.normal)
//        aboutBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//        contactBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//
//    }
//
//    @IBAction func contactAct(_ sender: Any) {
//       contactView .isHidden = false
//        aboutView.isHidden = true
//        faqLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        cntctLbl.backgroundColor = UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0)
//        aboutLbl.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue:249.0/255, alpha: 1.0)
//        contactBtn.setTitleColor(UIColor(red: 92.0/255, green: 179.0/255, blue:144.0/255, alpha: 1.0), for: UIControlState.normal)
//        faqBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//        aboutBtn.setTitleColor(UIColor .lightGray, for: UIControlState.normal)
//    }
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//
    @IBAction func btnCrossAct(_ sender: Any) {
            closeHandler?()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InformationVC: PopupContentViewController {
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
}
extension InformationVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellForInformationRow =  tableView.dequeueReusableCell(withIdentifier: "cellRow") as! cellForInformationRow
        
        switch indexPath.section {
        case 0:
            cell.labelForAnswer.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            break
        case 1:
            cell.labelForAnswer.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
            break
        default:
            cell.labelForAnswer.text = ""
            break
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arraySelctedHeader.count>0{
            if arraySelctedHeader.contains(section){
                return 1
            }
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : cellForInformation =  tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! cellForInformation
        
        switch section {
        case 0:
            cell.labelForQuestion.text = "What is Lorem lpsum?"
            break
        case 1:
            cell.labelForQuestion.text = "Why do we use it?"
            break
        default:
            cell.labelForQuestion.text = ""
            break
        }
        cell.btnForSelctedHeader.addTarget(self, action: #selector(tapOnHeader), for: .touchUpInside)
        cell.btnForSelctedHeader.tag = section+1
        return cell
    }
    
    @objc func tapOnHeader(_ sender: UIButton) {
        print(sender.tag-1)
        if arraySelctedHeader.contains(sender.tag-1) {
            arraySelctedHeader.remove(sender.tag-1)
        } else {
            arraySelctedHeader.add(sender.tag-1)
        }
        tableViewInfo.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

class cellForInformation: UITableViewCell {
    @IBOutlet weak var labelForQuestion: UILabel!
    @IBOutlet weak var imgViewDropdown: UIImageView!
    @IBOutlet weak var btnForSelctedHeader: UIButton!
}


class cellForInformationRow: UITableViewCell {
    @IBOutlet weak var labelForAnswer: UILabel!
}
