//
//  PageViewController.swift
//  iQadha
//
//  Created by netset on 10/03/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import Foundation
import UIKit
class PageViewController: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
	//MARK:- CONSTANT(S)
	var index = 0
	var identifiers = ["firstViewController", "secondViewController"]
	override func viewDidLoad() {
		self.dataSource = self
		self.delegate = self
		let startingViewController = self.viewControllerAtIndex(index: self.index)
		let viewControllers: NSArray = [startingViewController ?? ""]
		self.setViewControllers(viewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		for view in self.view.subviews{
			if view is UIScrollView{
				 view.frame = UIScreen.main.bounds
			}else if view is UIPageControl{
				view.backgroundColor = .clear
		}
	}
}

	override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	func viewControllerAtIndex(index: Int) -> UIViewController! {
		if index == 0 {
			return self.storyboard!.instantiateViewController(withIdentifier: "firstViewController")
		}
		if index == 1 {
			return self.storyboard!.instantiateViewController(withIdentifier: "secondViewController")
		}
		return nil
	}
	
	func presentationCount(for pageViewController: UIPageViewController) -> Int {
		return self.identifiers.count
	}
    
	func presentationIndex(for pageViewController: UIPageViewController) -> Int {
				guard let firstViewController = identifiers.first,
					let firstViewControllerIndex = identifiers.index(of: firstViewController) else {
					return 0
			}
				return firstViewControllerIndex
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		let identifier = viewController.restorationIdentifier
		let index = self.identifiers.index(of: identifier!)
		if index == 0 {
			return nil
		}
		self.index = self.index - 1
		return self.viewControllerAtIndex(index: self.index)
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		let identifier = viewController.restorationIdentifier
		let index = self.identifiers.index(of: identifier!)
		if index == identifiers.count - 1 {
			return nil
		}
		self.index = self.index + 1
		return self.viewControllerAtIndex(index: self.index)
	}
  }


