//
//  pagerVC.swift
//  iQadha
//
//  Created by NetSet on 4/5/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class PagerController: SJSegmentedViewController {
    
    var selectedSegment: SJSegmentTab?
    
    override func viewDidLoad() {
        
        navigationItem.title = "INFORMATION"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
        
        if let storyboard = self.storyboard {
            
            let firstViewController = storyboard
                .instantiateViewController(withIdentifier: "InformVC")
            firstViewController.title = "ABOUT"
         
            
            let secondViewController = storyboard
                .instantiateViewController(withIdentifier: "FaqVC")
            secondViewController.title = "FAQ"
            
            let thirdViewController = storyboard
                .instantiateViewController(withIdentifier: "ContactVC")
            thirdViewController.title = "CONTACT"
            
            headerViewController = nil
            segmentControllers = [firstViewController,
                                  secondViewController,
                                  thirdViewController
                                    ]
    
      
            selectedSegmentViewHeight = 2.0
            headerViewOffsetHeight = 31.0
            segmentTitleFont = UIFont(name: "ProximaNova-Semibold", size: 16.0)!
            segmentTitleColor = UIColor.init(hexString: "#888888")
            selectedSegmentViewColor = UIColor.init(hexString: "#72B192")
            segmentShadow = SJShadow.init()
            showsHorizontalScrollIndicator = false
            showsVerticalScrollIndicator = false
            segmentBounces = true
            segmentViewHeight = 50.0
            delegate = self
        }
        
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
             setSelectedSegmentAt(1, animated: true)
        
    }
    
    
}


extension PagerController: SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        
        if selectedSegment != nil {
            selectedSegment?.titleColor(UIColor.init(hexString: "#888888"))
        }
        
        if segments.count > 0 {
            selectedSegment = segments[index]
            selectedSegment?.titleColor(UIColor.init(hexString: "#72B192"))
        }
    }
}
