//
//  EditFasts.swift
//  iQadha
//
//  Created by NetSet on 3/21/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase


class EditFasts: BaseVC , UITextFieldDelegate{
    
    //MARK: Edit PopUp View
    @IBOutlet weak var viewEditPopUp: UIView!
    @IBOutlet weak var viewEditWhite: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtFldPrayer: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet var topTotalCount: UILabel!
    @IBOutlet weak var tableViewEditFast: UITableView!
    //MARK: Edit Qadha Screen
    @IBOutlet weak var lblTotalQadha: UILabel!
    
    var arrImages = [#imageLiteral(resourceName: "imageFast")]
    var arrTitle = ["Fasts Qadha"]
    var ref: DatabaseReference!
    var countFasts = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        txtFldPrayer.delegate = self
        navigationItem.title = "EDIT QADHA"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
       // UIApplication.shared.isStatusBarHidden = true
        
        let barBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem = barBtn
        self.setupPopUpViews()
        tableViewEditFast.tableFooterView = UIView.init()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if string == " " {
            return false
        } else if textField.text == "0" {
            textField.text! = ""
            return true
        } else {
            if textField.text!.count > 15 {
                
                if string.count == 0 {
                    return true
                } else {
                    return false
                }
            }
            return true
            
        }

        
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.viewEditPopUp.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getFastsData()
    }
    @objc func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        
        viewEditPopUp.frame = rect
        self.tabBarController?.view.addSubview(viewEditPopUp)
        viewEditPopUp.isHidden = true
        viewEditWhite.setViewCorner()
        btnSave.setCorner()
    }
    
    @IBAction func crossAction(_ sender: Any) {
        txtFldPrayer.resignFirstResponder()
        viewEditPopUp.isHidden = true
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        self.viewEditPopUp.endEditing(true)
        if txtFldPrayer.text! == "" {
            super.displayAlert(userMessage: Validation.call.enterFastValue)
        } else if txtFldPrayer.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterFastValue)
        } else {
        if InternetManager.isConnectedToNetwork() {
        let valuesForFasts = ["data":["1":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Fasts Qadha"] ]]
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
       // self.ref.child("fasts").child(userID as! String).setValue(valuesForFasts)
        
        self.ref.child("fasts").child(userID as! String).updateChildValues(valuesForFasts) { (error, ref) in
            if error == nil {
                
                self.getFastsData()
                
            } else{
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                print("error \(String(describing: error))")
            }
        }
        
        viewEditPopUp.isHidden = true
      }
        else {
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
            }
    }
    }
    
    func getFastsData() {
         if InternetManager.isConnectedToNetwork() {
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("fasts").child(userID as! String).child("data").observeSingleEvent(of: .value, with: { (snapshot) in
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                let dic = rest.value as! [String: Any]
                print(dic)
                self.countFasts = "\(dic["prayCount"] as! String)"
                self.topTotalCount.text! = "\(dic["prayCount"] as! String)"
            }
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            self.tableViewEditFast.delegate = self
            self.tableViewEditFast.dataSource = self
            self.tableViewEditFast.reloadData()
            
            
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
         }
         else {
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
//    override var prefersStatusBarHidden : Bool {
//        return true
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension EditFasts : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellEditFasts") as? customCell
        cell?.lblTitle.text = self.arrTitle[indexPath.row]
        cell?.imgView.image = arrImages[indexPath.row]
        cell?.lblCount.text! = "\(self.countFasts)"
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
        txtFldPrayer.text = "\(self.countFasts)"
          viewEditPopUp.isHidden = false
     
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 80.0
    }
}
