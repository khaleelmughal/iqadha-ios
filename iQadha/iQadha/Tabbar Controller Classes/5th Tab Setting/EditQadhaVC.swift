//
//  EditTotaliQadhaVC.swift
//  iQadha
//
//  Created by NetSet on 3/21/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

var chkForPrayerQadha:Bool = false

class EditQadhaVC: BaseVC , UITextFieldDelegate {
    
    //MARK: Edit PopUp View
    @IBOutlet weak var viewEditPopUp: UIView!
    @IBOutlet weak var viewEditWhite: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var txtFldPrayer: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var tableViewEditQadha: UITableView!
    //MARK: Edit Screen
    @IBOutlet weak var lblTotalQadha: UILabel!
    
    var arrImages = [#imageLiteral(resourceName: "Sun"),#imageLiteral(resourceName: "fullSun"),#imageLiteral(resourceName: "sunCloud"),#imageLiteral(resourceName: "orangeSun"),#imageLiteral(resourceName: "moonCloud"),#imageLiteral(resourceName: "moon")]
    var arrTitle = ["Fajr", "Zuhr", "Asr", "Maghrib", "Isha", "Witr"]
    var ref: DatabaseReference!
    var arrCountOfPrayers = NSMutableArray()
    var valuesForPrayers = [:] as NSDictionary
    
    var checkCount:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        txtFldPrayer.delegate = self
        navigationItem.title = "EDIT QADHA"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
        //UIApplication.shared.isStatusBarHidden = true
        
        let barBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonAction))
        self.navigationItem.leftBarButtonItem = barBtn
        
        self.setupPopUpViews()
        tableViewEditQadha.tableFooterView = UIView.init()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        totalQdha = 0
        chkForPrayerQadha = false
         getData()
        getReqQadha()
    }
    
    @objc func backButtonAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        
        viewEditPopUp.frame = rect
        self.tabBarController?.view.addSubview(viewEditPopUp)
        viewEditPopUp.isHidden = true
        viewEditWhite.setViewCorner()
        btnSave.setCorner()
    }
    
    
    
    @IBAction func crossAct(_ sender: Any) {
txtFldPrayer.resignFirstResponder()
        indexClicked = -1
        viewEditPopUp.isHidden = true
    }
    
    var saveAll:Bool = false
    var indexClicked:Int = -1
    var prayerInDB = ["Fajr" ,"Zuhr" , "Asr" ,"Magrib" , "Isha" , "Witr" ]
    
    @IBAction func saveAct(_ sender: Any) {
        
        print( Int(txtFldPrayer.text!))
        print(self.qadhaValInt)
        
        
        self.viewEditPopUp.endEditing(true)
        if txtFldPrayer.text?.count==0 {
            super.displayAlert(userMessage: Validation.call.enterQadhaValue)
            
        }
           
//            else if (txtFldPrayer.text?.count)!>10 {
////            super.displayAlert(userMessage: Validation.call.validCount )
////        }
            
            else if Int(txtFldPrayer.text!)! > self.qadhaValInt / 6 {
            super.displayAlert(userMessage: Validation.call.validQadhaPrayer )
            }
         else {
            
        if InternetManager.isConnectedToNetwork() {
        editQadhaCheck = true
        self.totalQdha = 0
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        if saveAll == false {
            valuesForPrayers = ["\(indexClicked as! Int + 1)": ["prayCount":"\(txtFldPrayer.text!)" , "prayName":prayerInDB[indexClicked] ]]
            self.ref.child("prayers").child(userID as! String).child("data").updateChildValues(valuesForPrayers as! [AnyHashable : Any])
            //getData()
        } else if saveAll == true {
            valuesForPrayers = ["data":
                ["1":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Fajr"] ,
                 "2":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Zuhr"] ,
                 "3":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Asr"]  ,
                 "4":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Magrib"] ,
                 "5":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Isha"] ,
                 "6":["prayCount":"\(txtFldPrayer.text!)" , "prayName":"Witr"]]
            ]
            self.ref.child("prayers").child(userID as! String).updateChildValues(valuesForPrayers as! [AnyHashable : Any])
            
            //getData()
        }
            //getData()
            chkForPrayerQadha  = true
        self.navigationController?.popViewController(animated: false)
        viewEditPopUp.isHidden = true
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
        }
    }
    
    var qadhaVal = String()
    var qadhaValInt = Int()
    
    func getReqQadha() {
        if InternetManager.isConnectedToNetwork() {
            CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
            let userID = UserDefaults.standard.value(forKey: "AuthId")!
            ref.child("users").child(userID as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                self.qadhaVal = value?["totalQadha"] as! String
                self.qadhaValInt = Int(self.qadhaVal)!
                DispatchQueue.main.async() {
                    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                }
            }) { (error) in
                DispatchQueue.main.async() {
                    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                }
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    @IBAction func applyToAllBtnAction(_ sender: UIButton) {
        if sender.isSelected{
            saveAll = false
            sender.isSelected = false
        }else{
            saveAll = true
            sender.isSelected = true
        }
    }
    var totalQdha = Int()
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        } else if textField.text == "0" {
            textField.text! = ""
             return true
        } else {
            if textField.text!.count > 15 {
                
                if string.count == 0 {
                    return true
                } else {
                    return false
                }
            }
                return true
            
        }
    }
    
    
    func getData() {
        
         if InternetManager.isConnectedToNetwork() {
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        
            if arrCountOfPrayers.count > 0 {
                
                arrCountOfPrayers.removeAllObjects()
            }
            
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("prayers").child(userID as! String).child("data").observeSingleEvent(of: .value, with: { (snapshot) in
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                let dic = rest.value as! [String: Any]
                self.arrCountOfPrayers.add(dic)
            }
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            self.totalQdha = 0
            for i in 0..<self.arrCountOfPrayers.count {
                let dict = self.arrCountOfPrayers[i] as! NSDictionary
                self.totalQdha = self.totalQdha + Int(dict["prayCount"] as! String)!
            }
            self.lblTotalQadha.text! = "\(self.totalQdha)"
            self.tableViewEditQadha.delegate = self
            self.tableViewEditQadha.dataSource = self
            self.tableViewEditQadha.reloadData()
            
        }) { (error) in
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
         } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension EditQadhaVC :UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? customCell
        let dict =  self.arrCountOfPrayers[indexPath.row] as! NSDictionary
        cell?.lblTitle.text = self.arrTitle[indexPath.row]
        cell?.lblCount.text! = "\(dict["prayCount"] as! String)"
        cell?.imgView.image = arrImages[indexPath.row]
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        checkCount = 0
        indexClicked = indexPath.row
        let dict =  self.arrCountOfPrayers[indexPath.row] as! NSDictionary
        txtFldPrayer.text! = "\(dict["prayCount"] as! String)"
        
        checkCount = Int("\(dict["prayCount"] as! String)")!
        
        lblHeader.text! = "Edit Qadha \(arrTitle[indexPath.row])"
        viewEditPopUp.isHidden = false
    }
}
