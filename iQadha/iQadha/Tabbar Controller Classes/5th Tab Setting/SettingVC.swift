//
//  SettingVC.swift
//  iQadha
//
//  Created by NetSet on 3/19/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class SettingVC: BaseVC , UITextFieldDelegate{
    
    //MARK: Input PopUp View
    @IBOutlet weak var viewInputPopUp: UIView!
    @IBOutlet weak var viewInputWhite: UIView!
    @IBOutlet weak var lblInputPopUpHeader: UILabel!
    @IBOutlet weak var txtFldInputPopUp: UITextField!
    @IBOutlet weak var btnSaveInputData: UIButton!
    
    //MARK: Gender View
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var viewGenderWhite: UIView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnSaveGender: UIButton!
    
    //MARK: SignOut View
    @IBOutlet var viewSignout: UIView!
    @IBOutlet weak var btnSignOut: UIButton!
    @IBOutlet weak var viewSignOutWhite: UIView!
    @IBOutlet weak var tableView: UITableView!
    var selectedCellStr = NSString()
    var PathIndex = NSIndexPath()
    var ref: DatabaseReference!
    let dictData: NSDictionary = ["Account":["Name", "Email", "Phone Number", "Gender", "Sign out"],
                                  "Notifications":["Qadha reminder"],
                                  "Edit Total":["Qadha", "Fasts"]
    ]
    var arrData  = ["","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtFldInputPopUp.addTarget(self, action:#selector(SettingVC.textValueChanges), for:.editingChanged)
        ref = Database.database().reference()
        txtFldInputPopUp.delegate = self
        self.txtFldInputPopUp.keyboardType = .asciiCapable
        //UIApplication.shared.isStatusBarHidden = true
        navigationItem.title = "SETTINGS"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navBg"), for: .default)
        self.setupPopUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserProfileData()
        
        if goToSettings == true {
            goToSettings = false
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditFasts") as! EditFasts
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
        if chkForPrayerQadha == true {
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditQadhaVC") as! EditQadhaVC
            self.navigationController?.pushViewController(secondViewController, animated: false)
        }
        self.view.endEditing(true)
    }
    
    var valueForNotifyButton = Bool()
    var strEmail = String()
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
          
    
            if lblInputPopUpHeader.text == "Name" && string == " " && textField.text?.count == 0 {
                return false
            }
            
            else  if lblInputPopUpHeader.text == "Name" && string == " " {
            return true
            }
            
            
            
            else {
                return false
            }
            
        } else {
            
            if lblInputPopUpHeader.text == "Name"
            {
                let ACCEPTABLE_CHARECTERS:NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
                let cs = CharacterSet.init(charactersIn: ACCEPTABLE_CHARECTERS as String).inverted
                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
                return (string == filtered)
            }
            else if lblInputPopUpHeader.text == "Email"
            {
                let ACCEPTABLE_CHARECTERS:NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@._!$^&*()"
                let cs = CharacterSet.init(charactersIn: ACCEPTABLE_CHARECTERS as String).inverted
                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
                return (string == filtered)
            }
            else if lblInputPopUpHeader.text == "Phone Number"
            {
                // tushar sir code
//                let ACCEPTABLE_CHARECTERS:NSString = "0123456789"
//                let cs = CharacterSet.init(charactersIn: ACCEPTABLE_CHARECTERS as String).inverted
//                let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
//                return (string == filtered)
                
                // aman
                let maxLength = 15
               
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if isBackSpace != -92 {
                    
                
                if textField.text?.count == 3 {
                    textField.text = textField.text! + "-"
                }
                
                
                if textField.text?.count == 7 {
                    textField.text = textField.text! + "-"
                }
                
                }
       
                
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
                
                
                
            }
            return true
        }
        
    }
    
    func getUserProfileData() {
         if InternetManager.isConnectedToNetwork() {
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("users").child(userID as! String).child("userProfile").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            self.arrData[0] = value?["name"] as? String  ?? ""
            self.arrData[1] = value?["email"] as? String ?? ""
            self.strEmail = value?["email"] as? String ?? ""
            self.arrData[3] = value?["gender"] as? String ?? ""
            self.arrData[2] = value?["phoneNumber"] as? String ?? ""
            self.valueForNotifyButton = value?["qadhaReminder"] as? Bool ?? false
            
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.reloadData()
            
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
         } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    func setupPopUpViews(){
        let window = UIWindow()
        let rect = CGRect(x: 0, y: 0, width: (window.frame.size.width), height: (window.frame.size.height))
        
        viewInputPopUp.frame = rect
        viewGender.frame = rect
        viewSignout.frame = rect
      
        self.tabBarController?.view.addSubview(viewInputPopUp)
        self.tabBarController?.view.addSubview(viewGender)
        self.tabBarController?.view.addSubview(viewSignout)
        
        viewInputWhite.setViewCorner()
        viewGenderWhite.setViewCorner()
        viewGenderWhite.setViewCorner()
        
        viewInputPopUp.isHidden = true
        viewGender.isHidden = true
        viewSignout.isHidden = true
        
        btnSaveInputData.setCorner()
        btnSaveGender.setCorner()
        btnSignOut.setCorner()
    }
    
    @IBAction func crossAction(_ sender: Any) {
        viewInputPopUp.isHidden = true
        viewGender.isHidden = true
        viewSignout.isHidden = true
        self.txtFldInputPopUp.endEditing(true)
    }
    
    @IBAction func signOutBtnAction(_ sender: Any) {
        viewSignout.isHidden = true
        
         if InternetManager.isConnectedToNetwork() {
            
        if Auth.auth().currentUser != nil {
            do {
                
                UserDefaults.standard.removeObject(forKey: "checkOnline")
                ref.child("users").child((Auth.auth().currentUser?.uid)!).updateChildValues(["online":false])
                
                try Auth.auth().signOut()
                
                UserDefaults.standard.removeObject(forKey: "profileCompleted")
                UserDefaults.standard.removeObject(forKey: "AuthId")
                clearDataOnLogout()
                let storyBoard = UIStoryboard(name: "Main", bundle: .main)
                let navVC = storyBoard.instantiateViewController(withIdentifier: "firstNavBar")
                self.present(navVC, animated: true, completion: nil)
                
            } catch let error as NSError {
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
        }
         } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    @IBAction func saveInputBtnAction(_ sender: UIButton) {
        self.viewInputPopUp.endEditing(true)
        if InternetManager.isConnectedToNetwork() {
        
        if lblInputPopUpHeader.text == "Email"{
            if txtFldInputPopUp.text?.count==0 {
                super.displayAlert(userMessage: Validation.call.enterEmail)
            } else if !checkEmailValidation(txtFldInputPopUp.text!){
                super.displayAlert(userMessage: Validation.call.validEmail)
            } else {
                txtFldInputPopUp.keyboardType = .emailAddress
                CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
                let user = Auth.auth().currentUser
                user?.updateEmail(to: txtFldInputPopUp.text!, completion: { (error) in
                    if error == nil {
                        print("suceedd.................")
                        self.updateEmail(value: self.txtFldInputPopUp.text! , param: "email")
                    } else if error != nil {
                         CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                        CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error!.localizedDescription)")
                        print("errorrrrrr.................\(error)")
                    }
                })
            }
        } else if lblInputPopUpHeader.text == "Name"{
            
            if txtFldInputPopUp.text?.count==0 {
                super.displayAlert(userMessage: Validation.call.enterName)
            } else {
                txtFldInputPopUp.keyboardType = .default
                updateName(value: txtFldInputPopUp.text!.trimmingCharacters(in: .whitespacesAndNewlines) , param: "name")
                viewInputPopUp.isHidden = true
                self.txtFldInputPopUp.endEditing(true)
            }
        } else if lblInputPopUpHeader.text == "Phone Number"{
            if txtFldInputPopUp.text?.count==0 {
                super.displayAlert(userMessage: Validation.call.enterNumber)
            } else if (txtFldInputPopUp.text?.count)!<6  {
                super.displayAlert(userMessage: Validation.call.enterValidNumber)
            } else {
            txtFldInputPopUp.keyboardType = .numberPad
            updateName(value: txtFldInputPopUp.text! , param: "phoneNumber")
                viewInputPopUp.isHidden = true
                self.txtFldInputPopUp.endEditing(true)
            }
        }
        
            self.view.endEditing(true)
            
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
        self.view.endEditing(true)
    }
    
    func updateEmail(value:String , param:String) {
         if InternetManager.isConnectedToNetwork() {
        let valuesForName = ["\(param)": "\(value)" ]
        print(valuesForName)
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        
        //self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName)
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName) { (error, ref) in
            if error == nil {
                
                if Auth.auth().currentUser != nil {
                    do {
                        try Auth.auth().signOut()
                        CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                        UserDefaults.standard.removeObject(forKey: "AuthId")
                        let storyBoard = UIStoryboard(name: "Main", bundle: .main)
                        let navVC = storyBoard.instantiateViewController(withIdentifier: "firstNavBar")
                        self.present(navVC, animated: true, completion: nil)
                        
                    } catch let error as NSError {
                        CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                        CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
                        print(error.localizedDescription)
                    }
                }
                
                
            } else{
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                print("error \(String(describing: error))")
            }
        }
         }  else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
       
    }
    
    func updateName(value:String , param:String) {
        if InternetManager.isConnectedToNetwork() {
        let valuesForName = ["\(param)": "\(value)" ]
        print(valuesForName)
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        
        //self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName)
        
        self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName) { (error, ref) in
            if error == nil {

            } else{
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                print("error \(String(describing: error))")
            }
        }
        getUserProfileData()
    } else {
    DispatchQueue.main.async() {
    // Hide Loading Indicator
    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
    }
    CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
    }
    }
    
    
    func updateNotification(value:Bool , param:String) {
         if InternetManager.isConnectedToNetwork() {
        let valuesForName = ["\(param)": value ]
        print(valuesForName)
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        
        //self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName)
        
        self.ref.child("users").child(userID as! String).child("userProfile").updateChildValues(valuesForName) { (error, ref) in
            if error == nil {
                let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.getNotificationStatus()
            } else{
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                print("error \(String(describing: error))")
            }
        }
        getUserProfileData()
    } else {
    DispatchQueue.main.async() {
    // Hide Loading Indicator
    CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
    }
    CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
    }
    }
    
    
    @IBAction func selectGenderBtnAction(_ sender: UIButton) {
        if sender.isEqual(btnFemale){
            btnMale.isSelected = false
            btnFemale.isSelected = true
        } else {
            btnFemale.isSelected = false
            btnMale.isSelected = true
        }
    }
    
    @IBAction func saveGenderBtnAction(_ sender: Any) {
        viewGender.isHidden = true
        if btnFemale.isSelected == false && btnMale.isSelected == true{
             updateName(value:"Male" , param:"gender")
        } else {
            updateName(value:"Female" , param:"gender")
        }
    }
    
//    override var prefersStatusBarHidden : Bool {
//        return true
//    }
    
    
    
    @objc func textValueChanges() {
        
    print(txtFldInputPopUp.text)

       
        
        
        
    }
    
    
    
    
}

extension SettingVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dictData.allKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key:String = dictData.allKeys[section] as! String
        let arr: NSArray = dictData.value(forKeyPath: key) as! NSArray
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let label = UILabel(frame: CGRect(x: 15, y: 10, width: tableView.frame.size.width, height: headerView.frame.height))
        label.textColor = UIColor(red: 92.0 / 255.0, green: 179.0 / 255.0, blue: 144.0 / 255.0, alpha: 1.0)
        if let aSize = UIFont(name: "Helvetica-Bold", size: 16.0) {
            label.font = aSize
        }
        label.text = dictData.allKeys[section] as? String
        label.backgroundColor = UIColor.white
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellSingleLabel") as? customCell
                cell?.selectionStyle = .none
                let arr: NSArray = dictData.value(forKeyPath: dictData.allKeys[indexPath.section] as! String) as! NSArray
                cell?.lblTitle.text = arr[indexPath.row] as? String
                return cell!
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTwoLabel") as? customCell
            cell?.selectionStyle = .none
            let arr: NSArray = dictData.value(forKeyPath: dictData.allKeys[indexPath.section] as! String) as! NSArray
            cell?.lblHeader.text = arr[indexPath.row] as? String
            if (arrData[indexPath.row] as! String) == "" {
            cell?.lblDetails.text = "Add"
            }
            
            else {
            cell?.lblDetails.text = arrData[indexPath.row]
                
            }
            
           
            return cell!
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSwitch") as? customCell
            cell?.selectionStyle = .none
            let arr: NSArray = dictData.value(forKeyPath: dictData.allKeys[indexPath.section] as! String) as! NSArray
            cell?.lblHeader.text = arr[indexPath.row] as? String
            
            if self.valueForNotifyButton == true {
                cell?.switchBtn.isOn = true
            } else {
                cell?.switchBtn.isOn = false
            }
            
            cell?.switchBtn.addTarget(self, action: #selector(switchBtnAction), for: .valueChanged)
            return cell!
        }
        else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSingleLabel") as? customCell
            cell?.selectionStyle = .none
            let arr: NSArray = dictData.value(forKeyPath: dictData.allKeys[indexPath.section] as! String) as! NSArray
            cell?.lblTitle.text = arr[indexPath.row] as? String
            return cell!
        }
        return UITableViewCell()
    }
    
    @objc func switchBtnAction(sender: UISwitch){
        if sender.isOn{
            updateNotification(value:true , param:"qadhaReminder")
            
        } else {
            updateNotification(value:false , param:"qadhaReminder")
            print("off")
        }
        print(sender)
    }
    
    
    func clearDataOnLogout() {
        // clr data of  date and baligh Date
        if let dob = userInfoGlobalDict["dob"] {
            userInfoGlobalDict["dob"] = nil
        }
        if let balighDate = userInfoGlobalDict["balighDate"] {
            userInfoGlobalDict["balighDate"] = nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            switch (indexPath.row){
            case 0:
                lblInputPopUpHeader.text = "Name"
              //  txtFldInputPopUp.placeholder = "Enter Name"
                txtFldInputPopUp.text = arrData[indexPath.row]
                txtFldInputPopUp.keyboardType = .default
                viewInputPopUp.isHidden = false
                break
            case 1:
                lblInputPopUpHeader.text = "Email"
               // txtFldInputPopUp.placeholder = "Enter email Address"
                txtFldInputPopUp.text = arrData[indexPath.row]
                txtFldInputPopUp.keyboardType = .emailAddress
                viewInputPopUp.isHidden = false
                break
            case 3:
                viewGender.isHidden = false
                break
            case 2:
                lblInputPopUpHeader.text = "Phone Number"
              //   txtFldInputPopUp.placeholder = "Enter Phone Number"
                txtFldInputPopUp.text = arrData[indexPath.row]
                txtFldInputPopUp.keyboardType = .numberPad
                viewInputPopUp.isHidden = false
                break
            case 4:
                viewSignout.isHidden = false
                break
            default:
                break
            }
        }
        else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditQadhaVC") as! EditQadhaVC
                self.navigationController?.pushViewController(secondViewController, animated: true)
               // self.performSegue(withIdentifier: "EditQadhaVC", sender: nil)
            }else {
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditFasts") as! EditFasts
                self.navigationController?.pushViewController(secondViewController, animated: true)
             //   self.performSegue(withIdentifier: "EditFasts", sender: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 4{
                return 50
            }
            return 65
        }else if indexPath.section == 1 {
            return 80
        }else if indexPath.section == 2 {
            return 56
        }
        return 0
    }
}

extension UIView{
    func setViewCorner(){
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 2.0
    }
}

extension UIButton{
    func setCorner(){
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 22.0
    }
}

