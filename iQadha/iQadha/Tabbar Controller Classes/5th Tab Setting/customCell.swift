//
//  nameCell.swift
//  iQadha
//
//  Created by NetSet on 3/21/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit

class customCell: UITableViewCell {
    
    //MARK: Settings Screen
    @IBOutlet var lblHeader: UILabel!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    
    //MARK: Edit IQadha Screen
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCount: UILabel!
    
    //MARK: Commons Outlet
    @IBOutlet weak var lblTitle: UILabel!
    
}
