//
//  EditPrayersVC.swift
//  iQadha
//
//  Created by Ranjana Prashar on 03/04/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
struct Constants {
    static let rowHeight = 50
    static let headerheight = 70
    static let footerHeight = 100
}

var globalEditArr:NSMutableArray = [0,0,0,0,0,0]

class EditPrayersVC: UIViewController {
    var closeHandler: (() -> Void)?
    
    @IBOutlet weak var tableViewCenterConst: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var tableViewEditPrayer: UITableView!
    var arrayData : [String] = ["FAJR","ZUHR","ASR","MAGHRIB","ISHA","WITR"]
    var arrayForImage : [UIImage] = [#imageLiteral(resourceName: "Sun"),#imageLiteral(resourceName: "fullSun"),#imageLiteral(resourceName: "sunCloud"),#imageLiteral(resourceName: "orangeSun"),#imageLiteral(resourceName: "moonCloud"),#imageLiteral(resourceName: "moon")]
    var maximumValues_HomeArray = NSMutableArray()
    var tapGuestureTimer:Timer!
    var selectedIndex = -1
    class func instance() -> EditPrayersVC {
        var storyboard = UIStoryboard()
        storyboard = UIStoryboard(name: "TabBar", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "EditPrayersID") as! EditPrayersVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.tabBarController?.selectedIndex == 1 {
            arrayData = ["FASTS"]
            arrayForImage = [#imageLiteral(resourceName: "imageFast")]
            globalEditArr = [0]
            self.tableViewCenterConst.constant = -120
        } else {
            print(maximumValues_HomeArray)
            globalEditArr = [0,0,0,0,0,0]
        }
        DispatchQueue.main.async {
            self.tableViewHeightConst.constant = CGFloat(Constants.headerheight + Constants.footerHeight + (Constants.rowHeight * self.arrayData.count))
            self.view.layoutIfNeeded()
        }
        tableViewEditPrayer.clipsToBounds=true
        tableViewEditPrayer.layer.cornerRadius=2
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension EditPrayersVC: PopupContentViewController {
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
}

extension EditPrayersVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EditPrayerRowCell =  tableView.dequeueReusableCell(withIdentifier: "rowCell") as! EditPrayerRowCell
        cell.imgVwForPrayer.image = arrayForImage[indexPath.row]
        cell.labelPrayerName.text = arrayData[indexPath.row]
        // cell.backgroundColor = .blue
        cell.btnAddPrayerCount.tag = indexPath.row+1000
        cell.btnMinusPrayerCount.tag = indexPath.row+2000
        cell.btnAddPrayerCount.addTarget(self, action: #selector(btnAddPrayerCount), for: .touchUpInside)
        cell.btnMinusPrayerCount.addTarget(self, action: #selector(btnMinusPrayerCount), for: .touchUpInside)
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(EditPrayersVC.addPrayerLongTap))
        cell.btnAddPrayerCount.addGestureRecognizer(longGesture)
        
        
        let longGestureReducePrayer = UILongPressGestureRecognizer(target: self, action: #selector(EditPrayersVC.reducePrayerLongTap))
        cell.btnMinusPrayerCount.addGestureRecognizer(longGestureReducePrayer)
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    //MARK: Long Tap Guesture
    @objc func addPrayerLongTap(_ sender: UILongPressGestureRecognizer) {
        guard let senderBtn = sender.view as? UIButton else {
            print("Sender is not a button")
            return
        }
        selectedIndex  = senderBtn.tag
        if (sender.state == UIGestureRecognizerState.ended) {
            print("Long press Ended");
            tapGuestureTimer.invalidate()
            tapGuestureTimer = nil
        } else if (sender.state == UIGestureRecognizerState.began) {
            print("Long press detected.");
            tapGuestureTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(EditPrayersVC.timerFires), userInfo: nil, repeats: true)
        }
    }
    
    @objc func reducePrayerLongTap(_ sender: UILongPressGestureRecognizer) {
        guard let senderBtn = sender.view as? UIButton else {
            print("Sender is not a button")
            return
        }
        selectedIndex  = senderBtn.tag
        if (sender.state == UIGestureRecognizerState.ended) {
            print("Long press Ended");
            tapGuestureTimer.invalidate()
            tapGuestureTimer = nil
        } else if (sender.state == UIGestureRecognizerState.began) {
            print("Long press detected.");
            tapGuestureTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(EditPrayersVC.timerFiresReducePrayer), userInfo: nil, repeats: true)
        }
    }
    
    
    
    //MARK: Timer Fires Add Here
    @objc func timerFires() {
        let indexPath = IndexPath(row: selectedIndex-1000, section: 0)
        let cell : EditPrayerRowCell = tableViewEditPrayer.cellForRow(at: indexPath) as! EditPrayerRowCell
        
        if self.tabBarController?.selectedIndex == 0 && Int(cell.labelShowPrayerCount.text!)! < Int(maximumValues_HomeArray[indexPath.row] as! String)! {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!+1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!+1)
        }
            
        else if self.tabBarController?.selectedIndex == 1 && Int(cell.labelShowPrayerCount.text!)! < Int(maximumValues_HomeArray[indexPath.row] as! String)! {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!+1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!+1)
        }
        
        
    }
    @objc func timerFiresReducePrayer() {
        let indexPath = IndexPath(row: selectedIndex-2000, section: 0)
        let cell : EditPrayerRowCell = tableViewEditPrayer.cellForRow(at: indexPath) as! EditPrayerRowCell
        if Int(cell.labelShowPrayerCount.text!)! > 0 {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!-1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!-1)
        }
        
    }
    
    
    
    @objc func addPrayerTap(_ sender: UIButton) {
        let indexPath = IndexPath(row: sender.tag-1000, section: 0)
        let cell : EditPrayerRowCell = tableViewEditPrayer.cellForRow(at: indexPath) as! EditPrayerRowCell
        globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!+1
        cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!+1)
        print(globalEditArr)
    }
    
    @objc func btnAddPrayerCount(_ sender: UIButton) {
        selectedIndex = sender.tag
        let indexPath = IndexPath(row: sender.tag-1000, section: 0)
        let cell : EditPrayerRowCell = tableViewEditPrayer.cellForRow(at: indexPath) as! EditPrayerRowCell
        
        if self.tabBarController?.selectedIndex == 0 && Int(cell.labelShowPrayerCount.text!)! < Int(maximumValues_HomeArray[indexPath.row] as! String)! {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!+1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!+1)
        }
            
        else if self.tabBarController?.selectedIndex == 1  && Int(cell.labelShowPrayerCount.text!)! < Int(maximumValues_HomeArray[indexPath.row] as! String)! {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!+1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!+1)
        }
        
        
        print(globalEditArr)
    }
    
    @objc func btnMinusPrayerCount(_ sender: UIButton) {
        selectedIndex = sender.tag
        let indexPath = IndexPath(row: sender.tag-2000, section: 0)
        let cell : EditPrayerRowCell = tableViewEditPrayer.cellForRow(at: indexPath) as! EditPrayerRowCell
        if Int(cell.labelShowPrayerCount.text!)! > 0 {
            globalEditArr[indexPath.row] = Int(cell.labelShowPrayerCount.text!)!-1
            cell.labelShowPrayerCount.text = String(Int(cell.labelShowPrayerCount.text!)!-1)
        }
        print(globalEditArr)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.rowHeight)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(Constants.headerheight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayForImage.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell : EditPrayerHeaderCell =  tableView.dequeueReusableCell(withIdentifier: "headerCell") as! EditPrayerHeaderCell
        let dateFormatterPrint = DateFormatter()
        //   cell.backgroundColor = .green
        dateFormatterPrint.dateFormat = "MMMM dd, yyyy"
        cell.labelForDate.text = dateFormatterPrint.string(from: Date())
        cell.btnCrossOut.addTarget(self, action: #selector(tapOnHeader), for: .touchUpInside)
        cell.btnCrossOut.tag = section+1
        return cell
    }
    
    @objc func tapOnHeader(_ sender: UIButton) {
        print(sender.tag-1)
        closeHandler?()
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(Constants.footerHeight)
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell : EditPrayerFooterCell =  tableView.dequeueReusableCell(withIdentifier: "footerCell") as! EditPrayerFooterCell
        cell.btnSave.addTarget(self, action: #selector(btnSaveAct), for: .touchUpInside)
        cell.btnSave.clipsToBounds=true
        let isDayMode = (UIApplication.shared.delegate as! AppDelegate).compareTimeDay_Night()
        cell.btnSave.backgroundColor = isDayMode ? UIColor.init(hexString: "#fbc742") : UIColor.init(hexString: "#6a549d")
        cell.btnSave.layer.cornerRadius = 22.0
        return cell
    }
    
    @objc func btnSaveAct(_ sender: UIButton) {
        
        print(sender.tag-1)
        closeHandler?()
        if self.tabBarController?.selectedIndex == 1 {
            NotificationCenter.default.post(name: Notification.Name("editFastsValues"), object: globalEditArr)
        } else {
            NotificationCenter.default.post(name: Notification.Name("editPrayerValues"), object: globalEditArr)
        }
        
    }
}

class EditPrayerHeaderCell: UITableViewCell {
    @IBOutlet weak var labelForDate: UILabel!
    @IBOutlet weak var btnCrossOut: UIButton!
}

class EditPrayerRowCell: UITableViewCell {
    @IBOutlet weak var imgVwForPrayer: UIImageView!
    @IBOutlet weak var labelPrayerName: UILabel!
    @IBOutlet weak var stackViewForCalculation: UIStackView!
    @IBOutlet weak var btnMinusPrayerCount: UIButton!
    @IBOutlet weak var labelShowPrayerCount: UILabel!
    @IBOutlet weak var btnAddPrayerCount: UIButton!
}

class EditPrayerFooterCell: UITableViewCell {
    @IBOutlet weak var btnSave: UIButton!
}

