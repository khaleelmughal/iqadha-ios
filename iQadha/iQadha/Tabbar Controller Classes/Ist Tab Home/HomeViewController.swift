//
//  HomeViewController.swift
//  iQadha
//
//  Created by netset on 10/03/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

var totalPrayersLoaderCount:Int = 0
var divReq = Float()
var editQadhaCheck:Bool = false

class BottomHomeCollVwCell : UICollectionViewCell {
    @IBOutlet var title: UILabel!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var img: UIImageView!
}

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    @IBOutlet weak var containerViewLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var btmCollVW: UICollectionView!
    @IBOutlet weak var prayQuadaBtn: UIButton!
    @IBOutlet var pgCntrl: UIPageControl!
    @IBOutlet weak var imgView: UIImageView!
    
    var arrayData = NSArray()
    var arrayForImage = NSArray()
    var arrChange : [Int] = [0,0,0,0,0,0]
    var totalPendingPrayers :Int = 0
    var ref: DatabaseReference!
    var arrCountOfPrayers = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        let serverTIme = ServerValue.timestamp()
        print(serverTIme)
        
        arrayData = ["FAJR","ZUHR","ASR","MAGHRIB","ISHA","WITR"]
        arrayForImage = [#imageLiteral(resourceName: "Sun"),#imageLiteral(resourceName: "fullSun"),#imageLiteral(resourceName: "sunCloud"),#imageLiteral(resourceName: "orangeSun"),#imageLiteral(resourceName: "moonCloud"),#imageLiteral(resourceName: "moon")]
        prayQuadaBtn.layer.masksToBounds = true
        prayQuadaBtn.layer.cornerRadius = 22.0
        self.pgCntrl.numberOfPages =  2
      
        
        UserDefaults.standard.removeObject(forKey: "checkOnline")
        let ud = "1"
        UserDefaults.standard.set("\(ud)", forKey: "checkOnline")
        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.removeObserver(Notification.Name("editPrayerValues"))
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.reloadPrayerValues), name: Notification.Name("editPrayerValues"), object: nil)
        
////        self.locationManager.requestAlwaysAuthorization()
////        self.locationManager.requestWhenInUseAuthorization()
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.startUpdatingLocation()
//        }
        
       //  getData() aman here
    }
    
    
    
    var updatedPrayerArr = NSMutableArray()
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
//    }
    
    var curentDate = NSDate()
    
    @objc func reloadPrayerValues(sender:Notification) {
        updatedPrayerArr.removeAllObjects()
        for i in 0..<arrCountOfPrayers.count {
            let dict = arrCountOfPrayers[i] as! NSDictionary
            
            if Int(dict["prayCount"] as! String)! < Int(globalEditArr[i] as! Int) {
                updatedPrayerArr.add(0)
            } else {
                updatedPrayerArr.add(Int(dict["prayCount"] as! String)! - Int(globalEditArr[i] as! Int))
            }
        }
        var valuesForPrayers = ["data":
            ["1":["prayCount":"\(String(describing: self.updatedPrayerArr[0]))" , "prayName":"Fajr"] ,
             "2":["prayCount":"\(String(describing: self.updatedPrayerArr[1]))" , "prayName":"Zuhr"] ,
             "3":["prayCount":"\(String(describing: self.updatedPrayerArr[2]))" , "prayName":"Asr"]  ,
             "4":["prayCount":"\(String(describing: self.updatedPrayerArr[3]))" , "prayName":"Magrib"] ,
             "5":["prayCount":"\(String(describing: self.updatedPrayerArr[4]))" , "prayName":"Isha"] ,
             "6":["prayCount":"\(String(describing: self.updatedPrayerArr[5]))" , "prayName":"Witr"]]
        ]
        print(valuesForPrayers)
        
//        self.ref.observe(.value, with: {
//            snap in
//            if let t = snap.value as? TimeInterval {
//                self.curentDate = NSDate(timeIntervalSince1970: t/1000)
//            }
//        })
//        print(self.curentDate)
//        let formatter = DateFormatter()
//        var dateStr = String()
//        formatter.dateFormat = "dd MMM,yyyy"
//        dateStr = formatter.string(from: self.curentDate as Date)
      
//        for i in 0..<globalEditArr.count {
//            co = co  + Int(globalEditArr[i] as! Int)
//        }
       // let valuesForActivity = ["\(dateStr as! String)" :["prayCount":co]]
        
        
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        self.ref.child("prayers").child(userID as! String).updateChildValues(valuesForPrayers)
      //  self.ref.child("activityLog").child(userID as! String).updateChildValues(valuesForActivity)
        valuesForPrayers = [:]
        updatedPrayerArr.removeAllObjects()
        //globalEditArr = [0,0,0,0,0,0]
        arrCountOfPrayers = []
//        getData()
          fetchPrayCountForActivity()
    }
    
    var countOfPrayersActivity = Int()
    
    func fetchPrayCountForActivity() {
        if InternetManager.isConnectedToNetwork() {

           // CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        
        self.ref.observe(.value, with: {
            snap in
            if let t = snap.value as? TimeInterval {
                self.curentDate = NSDate(timeIntervalSince1970: t/1000)
            }
        })
        print(self.curentDate)
        let formatter = DateFormatter()
        var dateStr = String()
        formatter.dateFormat = "dd MMM, yyyy"
        dateStr = formatter.string(from: self.curentDate as Date)
        
        ref.child("activityLog").child(userID as! String).child("\(dateStr)").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            self.countOfPrayersActivity = value?["prayCount"] as? Int ?? 0
            
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            print( self.countOfPrayersActivity)
            self.updatePrayerCountActivity()
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            print("...........")
            print( self.countOfPrayersActivity)
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    func updatePrayerCountActivity()  {
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        
        self.ref.observe(.value, with: {
            snap in
            if let t = snap.value as? TimeInterval {
                self.curentDate = NSDate(timeIntervalSince1970: t/1000)
            }
        })
        print(self.curentDate)
        let formatter = DateFormatter()
        var dateStr = String()
        formatter.dateFormat = "dd MMM, yyyy"
        dateStr = formatter.string(from: self.curentDate as Date)
        
        for i in 0..<globalEditArr.count {
             self.countOfPrayersActivity = self.countOfPrayersActivity  + Int(globalEditArr[i] as! Int)
        }
        let valuesForActivity = ["\(dateStr as! String)" :["prayCount":self.countOfPrayersActivity]]
        
        self.ref.child("activityLog").child(userID as! String).updateChildValues(valuesForActivity) { (error, ref) in
            if error == nil {
                
                editQadhaCheck = true
                
                globalEditArr = [0,0,0,0,0,0]
                self.getData()
            } else{
                CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error?.localizedDescription)")
                print("error \(String(describing: error))")
            }
        }
        
//        self.ref.child("activityLog").child(userID as! String).updateChildValues(valuesForActivity)
//        globalEditArr = [0,0,0,0,0,0]
//        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // aman here first it was in did load
          setContainerViewFrame(constValue: self.view.frame.size.width)
         let isDayMode = (UIApplication.shared.delegate as! AppDelegate).compareTimeDay_Night()
        imgView.image = isDayMode ? #imageLiteral(resourceName: "DayImage") : #imageLiteral(resourceName: "moonUp")
        imgView.backgroundColor = isDayMode ? UIColor.init(hexString: "#fbc742") : UIColor.init(hexString: "#6a549d")
        prayQuadaBtn.backgroundColor = isDayMode ? UIColor.init(hexString: "#fbc742") : UIColor.init(hexString: "#6a549d")
        self.view.endEditing(true)
        
//        if editQadhaCheck == true {
            getData()
//        }
    }
    
    func getData() {
        
        if InternetManager.isConnectedToNetwork() {
        
        self.arrCountOfPrayers.removeAllObjects()
        CommonFunctions.sharedCommonFunctions.activateView(self.view, loaderText: "loading..")
        
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("prayers").child(userID as! String).child("data").observeSingleEvent(of: .value, with: { (snapshot) in
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                let dic = rest.value as! [String: Any]
                self.arrCountOfPrayers.add(dic)
            }
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                     CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            totalPrayersLoaderCount = 0
            for i in 0..<self.arrCountOfPrayers.count {
                let dict = self.arrCountOfPrayers[i] as! NSDictionary
                totalPrayersLoaderCount = totalPrayersLoaderCount + Int(dict["prayCount"] as! String)!
            }
            
            if editQadhaCheck == true {
                NotificationCenter.default.post(name: Notification.Name("reloadProgressLoaderValue"), object: totalPrayersLoaderCount)
            }
            
            if editQadhaCheck == true {
                editQadhaCheck = false
            }
            
            self.btmCollVW.delegate = self
            self.btmCollVW.dataSource = self
            self.btmCollVW.reloadData()
            
        }) { (error) in
            DispatchQueue.main.async() {
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
        } else {
            DispatchQueue.main.async() {
                // Hide Loading Indicator
                CommonFunctions.sharedCommonFunctions.inactivateView(self.view)
            }
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: internetNotAvailableTitle, message: internetNotAvailableMsg)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0.1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! BottomHomeCollVwCell
            let dict =  self.arrCountOfPrayers[indexPath.row] as! NSDictionary
        
         myCell.title.text! = arrayData[indexPath.row] as! String
           //myCell.title.text! = dict["prayName"] as! String
//            myCell.title.text! = arrayData[indexPath.row] as! String
//            let dig = Int(myCell.lblCount.text!)
//            let num = dig! - arrChange[indexPath.row]
//            myCell.lblCount.text! = "\(num)"
            myCell.lblCount.text! = "\(dict["prayCount"] as! String)"
            myCell.img.image! = arrayForImage[indexPath.row] as! UIImage
//            totalPendingPrayers = totalPendingPrayers + num
            return myCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width:collectionView.frame.size.width/4, height:(collectionView.frame.size.width/4)+10)
    }
    
    var currentPage = 0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.width
        self.currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        self.pgCntrl.currentPage = self.currentPage
    }
    
    @IBAction func prayBtnAction(_ sender: Any) {
       // blurVw.isHidden = false
       // firstView.isHidden = false
        arrChange  = [0,0,0,0,0,0]
        //tblVw.reloadData()
        self.showPopUp()
    }

    func showPopUp() {
        let popup = PopupController
            .create(self.tabBarController!)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.6)),
                    .dismissWhenTaps(false),
                    .scrollable(false),
                    .movesAlongWithKeyboard(false)
                ]
            )
            .didShowHandler { popup in
        }
        let container = EditPrayersVC.instance()
        
     // create required array data To send To Next Page
        var arrayToPass = NSMutableArray()
        for i in 0..<arrCountOfPrayers.count {
            arrayToPass.add((arrCountOfPrayers[i] as! NSDictionary).value(forKey: "prayCount") as! String)
        }
       print(arrayToPass)
        container.maximumValues_HomeArray = arrayToPass
        container.closeHandler = {
            popup.dismiss()
        }
        popup.show(container)
    }
    
    
    @IBAction func swipeGestureOnImgVw(_ sender: Any) {
        let swipeGesture = sender as! UISwipeGestureRecognizer
        UIView.animate(withDuration: 0.7, animations: { [weak self] in
            if swipeGesture.direction == .right {
                // right
               // totalPrayersLoaderCount = 0
                self?.setContainerViewFrame(constValue: (self?.view.frame.size.width)!)
                self?.pgCntrl.currentPage = 0
                // progressView2.show(withProgress: 0.0, on: self.containerView)
            } else {
                // left
                NotificationCenter.default.post(name: Notification.Name("reloadProgressLoaderValue"), object: totalPrayersLoaderCount)
               // totalPrayersLoaderCount = 0
                self?.setContainerViewFrame(constValue: (self?.view.frame.origin.x)!)
                self?.pgCntrl.currentPage = 1
                
                //progressView2.show(withProgress: 0.45, on: self.containerView)
            }
            },  completion: {(finished: Bool) in
                if swipeGesture.direction == .left {
                    progressView2.setBarProgress(CGFloat(divReq), animate: true)
                } else {
                    progressView2.setBarProgress(0.00, animate: false)
                }
        })
    }
    
    @IBAction func pageCtrlAct(_ sender: Any) {
        if (sender as! UIPageControl).currentPage == 1 {
            UIView.animate(withDuration: 0.7, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:{ [weak self] in
                // left
                self?.setContainerViewFrame(constValue: (self?.view.frame.origin.x)!)
                self?.pgCntrl.currentPage = 1
                },  completion: {(finished: Bool) in
                    progressView2.setBarProgress(CGFloat(divReq), animate: true)
            })
        }
    }
    
    func setContainerViewFrame(constValue:CGFloat) {
        containerViewLeadingConst.constant = constValue
        self.view.layoutIfNeeded()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
