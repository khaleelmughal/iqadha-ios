 //
//  ProgressIndicatorVC.swift
//  iQadha
//
//  Created by Ranjana Prashar on 04/04/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

var progressView2 = BSProgressSpeedIndicator()
class ProgressIndicatorVC: UIViewController {
    @IBOutlet weak var progressView1: UIView!    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblValuedOfPrayer: UILabel!
    var progressView = BSProgressSpeedIndicator()
    var totalPrayers = Int()
    var totalCount = Int()
    var ref: DatabaseReference!
    var req :Int = 0
    var curentDate = NSDate()
    var globalVal = Int()
    var globalReq = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        ref.observe(.value, with: {
            snap in
            if let t = snap.value as? TimeInterval {
                self.curentDate = NSDate(timeIntervalSince1970: t/1000)
            }
        })
        print(curentDate)
        
      //  EEEE MMMM dd, yyyy’ at ‘h:mm a zz.       Tuesday January 09, 2007 at 10:00 AM PST.
        
        let formatter = DateFormatter()
//        // initially set the format based on your datepicker date
        formatter.dateFormat = "MMMM dd, yyyy EEEE"
        lblDate.text! = formatter.string(from: curentDate as Date)

        
        let timestamp = Int(curentDate.timeIntervalSince1970 * 1000)
        print(timestamp)
        
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        let values = ["onlineTime":timestamp ]
        self.ref.child("users").child(userID as! String).updateChildValues(values)
        
      //  NotificationCenter.default.removeObserver(Notification.Name("reloadProgressLoaderValue"))
        NotificationCenter.default.addObserver(self, selector: #selector(ProgressIndicatorVC.reloadView), name: Notification.Name("reloadProgressLoaderValue"), object: nil)
        
        self.view.backgroundColor = .clear
      //  getPrayers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // aman here  move from did load
          getPrayers()
        print("appear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       // loaderVw()
    }
    
    var val = Int()
    @objc func reloadView(sender:Notification) {
        print(val,req,Int(sender.object as! Int))
        val = req - Int(sender.object as! Int)
        globalVal = val
        globalReq = req
        if val < 0 {
       lblValuedOfPrayer.text! = "\(0) of \(req) Completed"
        }
        else {
        lblValuedOfPrayer.text! = "\(val) of \(req) Completed"
        }
        loaderVw()
    }
    
    func loaderVw() {
        DispatchQueue.main.async {
            if self.progressView.frame.size.height == 0{
                self.progressView = BSProgressSpeedIndicator.init(frame: CGRect(x: self.progressView1.frame.origin.x, y: self.progressView1.frame.origin.y+(self.progressView1.frame.size.height/2.5), width: self.progressView1.frame.size.width, height: self.progressView1.frame.size.height))
                progressView2 = BSProgressSpeedIndicator.init(frame: CGRect(x: self.progressView1.frame.origin.x-10, y: self.progressView1.frame.origin.y+(self.progressView1.frame.size.height/2.5), width: self.progressView1.frame.size.width+20, height: self.progressView1.frame.size.height))
                  let isDayMode = (UIApplication.shared.delegate as! AppDelegate).compareTimeDay_Night()
                self.progressView.defInit(2.0, boolDayNight: isDayMode, boolViewType: false)
                progressView2.defInit(12.0, boolDayNight: isDayMode, boolViewType: true)
            }
            var div = (Float(self.val) / Float(self.req))
            if div < 0.0 {
               div = 0.0
            }
            print(div)
            divReq = div
            self.progressView.show(withProgress: 0.0, on: self.view)
//             self.progressView.show(withProgress: 0.0, on: self.view)
            
            progressView2.show(withProgress: CGFloat(div), on: self.view)
            progressView2.setBarProgress(CGFloat(div), animate: false)
        }
    }
    
    func getPrayers() {
        let userID = UserDefaults.standard.value(forKey: "AuthId")!
        ref.child("users").child(userID as! String).child("totalQadha").observeSingleEvent(of: .value, with: { (snapshot) in
            let valStr = snapshot.value as! String
            let val = Int(valStr as! String)
            self.req = Int(val!) // it was multiply by 6 earlier.
           print(snapshot)
            
        }) { (error) in
            CommonFunctions.sharedCommonFunctions.showAlert(self, title: "Error", message: "\(error.localizedDescription)")
            print(error.localizedDescription)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
