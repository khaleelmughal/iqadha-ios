//
//  TabbarController.swift
//  iQadha
//
//  Created by Ranjana Prashar on 06/04/18.
//  Copyright © 2018 Netset. All rights reserved.
//

import UIKit
import CoreLocation

class TabbarController: UITabBarController,UITabBarControllerDelegate,CLLocationManagerDelegate {
    
  let locationManager = CLLocationManager()
    var selectedTabIndex:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
           self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
  print(tabBar.items?.index(of: item))
  selectedTabIndex = tabBar.items?.index(of: item)
    }

    
 
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        print("%i",tabBarController.selectedIndex)
      
        if tabBarController.selectedIndex != 2 && selectedTabIndex == 2 {
     locationManager.delegate = self
            if  CLLocationManager.authorizationStatus() == .denied {
       
                let alertController = UIAlertController(title: "iQadha", message: "GPS location service is disabled on your device. Go to device settings and enable location to see charts, activity log and live view.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    let url = URL(string: UIApplicationOpenSettingsURLString)
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    print("Cancel Pressed")
                }
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
               return false
          
                
            }
            else {
       return true
            }
        }
        
        else {
        return true
        }
    }
    
    //MARK:- Check Location Method:--
    func checkForLocation_ActivityTab() {
    locationManager.delegate = self
        switch CLLocationManager.authorizationStatus()  {
        case .denied:
            let alertController = UIAlertController(title: "iQadha", message: "GPS location service is disabled on your device. Go to device settings and enable location to see charts, activity log and live view.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                let url = URL(string: UIApplicationOpenSettingsURLString)
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                print("Cancel Pressed")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
            //            self.alertMessageWithAction(title: "iQadha", message: "Please enable location", viewcontroller: self, action: move_to_settingToEnableLocation)
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        
        case .notDetermined:
              locationManager.startUpdatingLocation()
        case .restricted:
              locationManager.startUpdatingLocation()
        
        case .authorizedAlways:
             locationManager.startUpdatingLocation()
            
        case .authorizedWhenInUse:
             locationManager.startUpdatingLocation()
        }
    
}
}
