//
//  Validation.swift
//  Adeeb
//
//  Created by Apple on 09/12/17.
//  Copyright © 2017 netset. All rights reserved.
//

import UIKit

class Validation: NSObject {
    struct call {
        // Login
        static let enterEmail = "Please enter your email."
        static let validEmail = "Please enter a valid email address."
        static let enterPassword = "Please enter your password."
        static let strongPassword = "Strong passwords have at least 6 characters and a mix of letters and numbers."
        static let countPassword = "Password should be atleast 6 character long."
        static let selectGender = "Please select gender."
        static let enterName = "Please enter your name."
        static let enterNumber = "Please enter your number."
        static let enterValidNumber = "Please enter valid phone number only."
        static let enterQadhaValue = "Please enter value."
        static let enterFastValue = "Please enter value."
        static let validCount = "Please enter a valid value."
        static let validQadhaPrayer = "Invalid prayers. Entered prayers should be less than total target."
        // Forgot Password
        static let registeredEmail = "Please enter your registered email address."
        
        // SignUp
        
        static let firstName = "Please enter your first name"
        static let lastName = "Please enter your last name"
        
        // Request Book
        static let bookTitle = "Please enter book title"
        static let enterWriterName = "Please enter author name"
        static let selectCategory = "Please enter subject/category"
        
        
        // Add new card
        static let name = "Please enter your name"
        static let cardNumber = "Please enter card number"
        static let validCardNumber = "Please enter valid card number"
        static let expiryMonth = "Please select expiry month"
        static let expiryYear = "Please select expiry year"
        static let cvv = "Please enter cvv"
        
        // Change Password
        static let enterCurrentPassword = "Please enter your current password"
        static let enterNewPassword     = "Please enter your new password"
        static let passwordMaxLength    = "Password must contain 8 characters"
        static let enterConfirmPassword = "Please enter confirm new password"
        static let passwordNotMatch     = "Password and confirm password do not match"
        
        // Add New Address
        
        static let enterFlatName = "Please enter building name"
        static let enterLandmark = "Please enter landmark"
        static let enterLocation = "Please choose location"
        static let enterTagAddress = "Please enter tag address"
        
        // Select Adddress
        static let addCard = "Please add card to process payment"
        static let selectCard = "Please select card to process payment"
        
        // Save Book Review Locally
        static let feedback = "Your response has been recorded. We appreciate your time and value your opinions."
        
        // Feedback Screen
        
        static let giveRating = "Please give rating"
        static let addComment = "Please add comment"
        
        //Select Address Screen
        
        static let addAddress = "Please add address to process payment"
        static let selectAddress = "Please select address to proceed payment"
    }
    
    struct row {
        static let height = 160.0
    }
    
    struct notificationRow {
        static let ipadHeight = 90.0
        static let height = 60.0
    }
    
    struct headerHeight {
        static let iphone = 40.0
        static let ipad = 60.0
    }
}
